using System.Collections;
using System.Collections.Generic;

namespace arqsi2018.Models.Restriction
{
    public class CompositeRestrictionStrategy  : IRestrictionStrategy, IEnumerable
    {
        private readonly List<IRestrictionStrategy> _validations = new List<IRestrictionStrategy>();
        
        public bool ValidateRestrictions(Restriction restricton)
        {
            foreach (var validation in _validations)
            {
                if(!validation.ValidateRestrictions(restricton))
                    return false;
            }

            return true;
        }

        public void Add(IRestrictionStrategy validation)
        {
            _validations.Add(validation); 
        }

        public void Remove(IRestrictionStrategy validation)
        {
            _validations.Remove(validation); 
        }
        
        public IRestrictionStrategy GetValidation(int index)
        {
            return _validations[index];
        }
        
        private IEnumerator<IRestrictionStrategy> GetEnumerator()
        {
            foreach (var validation in _validations)
            {
                yield return validation;
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}