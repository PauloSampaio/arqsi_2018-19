namespace arqsi2018.Models.Restriction
{
    public class RestrictionByPercentageStrategy : IRestrictionStrategy
    {
        public bool ValidateRestrictions(Restriction restriction)
        {
            if (restriction.MinDimensions > 0 && restriction.MaxDimensions > 0 &&
                restriction.MinDimensions > restriction.MaxDimensions)
                return false;

            if (restriction.MinDimensions > 0)
            {
                // if part fills less than the target min percentage of base return false
                if (!restriction.Part.CheckFillsMinPct(restriction.Product.ProductDimensions,
                    restriction.MinDimensions))
                    return false;
            }
            
            if(restriction.MaxDimensions > 0)
            {
                // if part fills more than the target max percentage of base return false
                if (!restriction.Part.CheckFillsMaxPct(restriction.Product.ProductDimensions,
                    restriction.MaxDimensions))
                    return false;
            }

            return true;
        }
    }
}