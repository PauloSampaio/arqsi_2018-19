using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Services;
using arqsi2018.Models.Catalog;
using arqsi2018.Models.Category;
using arqsi2018.Models.Dimensions;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Product;
using arqsi2018.Models.Restriction;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace arqsi2018.Data.Repositories
{
    public class ProductRepo : RepoBase<ProductReadDto, ProductWriteDto, Product>
    {
        private readonly ApiContext _context;
        private readonly ProductAssembler _assembler;

        public ProductRepo(ApiContext context)
        {
            _context = context;
            _assembler = new ProductAssembler();
        }
        
        /**
         *
         ***************************************
         *            GET_ALL                  *
         ***************************************
         * 
         */
        public IEnumerable<ProductReadDto> GetAll()
        {
            return _context.Products
                .Include(x => x.Category)
                .Include(x => x.ProductDimensions)
                .Include(x => x.ProductCatalogs)
                .Include(x => x.ProductMaterials)
                .Include(x => x.OwnedParts)
                .Select(x => _assembler.ToDto(x)).ToList();
        }
        
        /**
         *
         ***************************************
         *        GET_ALL_RESTRICTIONS         *
         ***************************************
         * 
         */
        public IEnumerable<RestrictionReadDto> GetAllRestrictions()
        {
            return _context.Restrictions
                .Include(x => x.Product)
                .Include(x => x.Part)
                .Select(x => _assembler.ToRestrictionDto(x)).ToList();
        }

        /**
         *
         ***************************************
         *            POST                     *
         ***************************************
         * 
         */
        public Product Add(ProductWriteDto writeDto)
        {
            var product = _assembler.ToDomainObject(writeDto);

            product.Category = GetCategory(writeDto);
            product.ProductDimensions = writeDto.Dimensionses == null ? new List<ProductDimensions>() : GetDimensions(product, writeDto);
            product.ProductCatalogs = writeDto.Catalogs == null ? new List<ProductCatalog>() : GetCatalogs(product, writeDto);
            product.ProductMaterials = writeDto.Materials == null ? new List<ProductMaterial>() : GetMaterials(product, writeDto);
            product.ParentProducts = new List<Restriction>();
            
            //product parts
            product.OwnedParts = writeDto.PartsIds == null ? new List<Restriction>() : GetRestricions(product, writeDto);
            
            var result = _context.Add(product).Entity;
            return result;
        }

        /**
         *
         ***************************************
         *            GET_BY_ID                *
         ***************************************
         * 
         */
        public ProductReadDto GetById(int id)
        {
            return _assembler.ToDto(GetProductById(id));
        }
        
        /**
         *
         ***************************************
         *       GET_BY_ID / OWNED_PARTS       *
         ***************************************
         * 
         */
        public ProductPartsReadDto GetByIdParts(int id)
        {
            return _assembler.ToPartsDto(GetProductById(id));
        }
        
        /**
         *
         ***************************************
         *         GET_BY_ID / PART_OF         *
         ***************************************
         * 
         */
        public ProductParentsReadDto GetByIdPartOf(int id)
        {
            return _assembler.ToParentsReadDto(GetProductById(id));
        }
        
        /**
         *
         ***************************************
         *       GET_RESTRICTIONS_BY_ID        *
         ***************************************
         * 
         */
        public IEnumerable<RestrictionReadDto> GetByIdRestrictions(int id)
        {
            return _context.Restrictions
                .Include(x => x.Product)
                .Include(x => x.Part)
                .Where(x => x.ProductId == id)
                .Select(x => _assembler.ToRestrictionDto(x)).ToList();
        }
        
        /**
         *
         ***************************************
         *    GET_RESTRICTIONS_BY_COMP_KEY     *
         ***************************************
         * 
         */
        public RestrictionReadDto GetByCompKeyRestriction(int id, int idpart)
        {
            return _context.Restrictions
                .Include(x => x.Product)
                .Include(x => x.Part)
                .Where(x => x.ProductId == id).Where(x => x.PartId == idpart)
                .Select(x => _assembler.ToRestrictionDto(x)).ToList().FirstOrDefault();
        }

        /**
         *
         ***************************************
         *            GET_BY_NAME              *
         ***************************************
         * 
         */
        public ProductReadDto GetByName(string name)
        {
            return _assembler.ToDto(GetProductByName(name));
        }

        
        
        /**
         *
         ***************************************
         *                PUT                  *
         ***************************************
         * 
         */
        public ProductReadDto UpdateElement(int id, ProductWriteDto writeDto)
        {
            var toUpdate = GetProductById(id);

            if (writeDto.Name != null)
            {toUpdate.Name = writeDto.Name;}

            if (writeDto.Description != null)
            {toUpdate.Description = writeDto.Description;}

            if (writeDto.Reference != null)
            {toUpdate.Reference = writeDto.Reference;}
            
            if(writeDto.CategoryId.HasValue)
            {toUpdate.Category = GetCategory(writeDto);}

            // cleanup for editting
            toUpdate.ProductDimensions = new List<ProductDimensions>();
            toUpdate.ProductCatalogs = new List<ProductCatalog>();
            toUpdate.ProductMaterials = new List<ProductMaterial>();
            toUpdate.OwnedParts = new List<Restriction>();
            _context.SaveChanges();

            if (writeDto.Dimensionses != null)
            {toUpdate.ProductDimensions = GetDimensions(toUpdate, writeDto);}
    
            if(writeDto.Catalogs != null)
            {toUpdate.ProductCatalogs = GetCatalogs(toUpdate, writeDto);}
            
            if(writeDto.Materials != null)
            {toUpdate.ProductMaterials = GetMaterials(toUpdate, writeDto);}
            
            if(writeDto.PartsIds != null)
            {toUpdate.OwnedParts = GetRestricions(toUpdate, writeDto);}
            
            toUpdate.ParentProducts = new List<Restriction>();

            _context.SaveChanges();
            return _assembler.ToDto(toUpdate);
        }

        /**
         *
         ***************************************
         *                DEL                  *
         ***************************************
         * 
         */
        public ProductReadDto DeleteElement(int id)
        {
            var toDelete = GetProductById(id);

            foreach (var restriction in toDelete.OwnedParts)
            {
                _context.Restrictions.Remove(restriction);
            }

            foreach (var proddimension in toDelete.ProductDimensions)
            {
                _context.Dimensionses.Remove(proddimension.Dimensions);
            }

            _context.SaveChanges();
            
            _context.Products.Remove(toDelete);
            _context.SaveChanges();
            return _assembler.ToDto(toDelete);
        }
        
        /// <summary>
        /// Get Product By Id - private aux method
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>Product</returns>
        public Product GetProductById(int id)
        {
            
            return _context.Products
                .Include(x => x.Category)
                .Include(x => x.ProductCatalogs).ThenInclude(x => x.Catalog)
                .Include(x => x.ProductDimensions).ThenInclude(x => x.Dimensions)
                .Include(x => x.OwnedParts)
                .Include(x => x.ProductMaterials).ToList()
                .FirstOrDefault(x => x.ProductId == id);
        }  
        
        /// <summary>
        /// Get Dimensions - private aux method
        /// </summary>
        /// <param name="product"></param>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        private ICollection<ProductDimensions> GetDimensions(Product product, ProductWriteDto writeDto)
        {
            var productDimensions = new List<ProductDimensions>();

            foreach (var id in writeDto.Dimensionses)
            {
                var dimensions = _context.Dimensionses.Find(id);
                var productDimension = _context.ProductDimensions.Add(
                    new ProductDimensions
                    {
                        Dimensions = dimensions,
                        DimensionsId = dimensions.DimensionsId,
                        Product = product,
                        ProductId = product.ProductId
                    }
                ).Entity;
                _context.SaveChanges();
                
                productDimensions.Add(productDimension);
            }

            return productDimensions;
        }
        

        /// <summary>
        /// Get Category - private aux method
        /// </summary>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        private Category GetCategory(ProductWriteDto writeDto)
        {
            return _context.Categories.Find(writeDto.CategoryId);
        }
        
        /// <summary>
        /// Get Catalogs - private aux method
        /// </summary>
        /// <param name="product"></param>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        private ICollection<ProductCatalog> GetCatalogs(Product product, ProductWriteDto writeDto)
        {
            var productCatalogs = new List<ProductCatalog>();

            foreach (var id in writeDto.Catalogs)
            {
                var catalog = _context.Catalogs.Find(id);
                var productCatalog = _context.ProductCatalogs.Add(
                    new ProductCatalog
                    {
                        Catalog = catalog,
                        CatalogId = catalog.CatalogId,
                        Product = product,
                        ProductId = product.ProductId
                    }
                ).Entity;
                _context.SaveChanges();
                
                productCatalogs.Add(productCatalog);
            }

            return productCatalogs;
        }

        /// <summary>
        /// Get Materials - private aux method
        /// </summary>
        /// <param name="product"></param>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        private ICollection<ProductMaterial> GetMaterials(Product product, ProductWriteDto writeDto)
        {
            var productMaterials = new List<ProductMaterial>();

            foreach (var id in writeDto.Materials)
            {
                var material = _context.Materials.Find(id);
                var productMaterial = _context.ProductMaterials.Add(
                    new ProductMaterial
                    {
                        Material = material,
                        MaterialId = material.MaterialId,
                        Product = product,
                        ProductId = product.ProductId
                    }
                ).Entity;
                _context.SaveChanges();
                
                productMaterials.Add(productMaterial);
            }

            return productMaterials;
        }

        /// <summary>
        /// Get Restrictions - private aux method
        /// </summary>
        /// <param name="product"></param>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        private ICollection<Restriction> GetRestricions(Product product, ProductWriteDto writeDto)
        {
            var restrictionsData = _assembler.ToRestrictionsPartialDto(writeDto);  
            var restrictions = new List<Restriction>();
            
            using(var e1 = writeDto.PartsIds.GetEnumerator())
            using(var e2 = restrictionsData.GetEnumerator()) 
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    var partid = e1.Current;
                    var restdata = e2.Current;
                    
                    var productPart = GetProductById(partid);

                    if (!productPart.CheckFitsIn(product.ProductDimensions)) continue; // caber

                    var restriction = new Restriction
                    {
                        Part = productPart,
                        PartId = productPart.ProductId,
                        Product = product,
                        ProductId = product.ProductId,
                        IsRequired = restdata.IsRequired,
                        UseBaseProductMaterial = restdata.UseBaseProductMaterial,
                        MinDimensions = restdata.MinDimensions,
                        MaxDimensions = restdata.MaxDimensions
                    };

                    if (!restriction.ValidateRestrictions()) continue; // validates restriction strategies

                    var ownedPart = _context.Restrictions.Add(restriction).Entity;
                    _context.SaveChanges();
                    restrictions.Add(ownedPart);
                }
            }
            return restrictions;
        }
        
        /// <summary>
        /// Get Product By Name - private aux method
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>Product</returns>
        private Product GetProductByName(string Name)
        {
            return _context.Products
                .Include(x => x.ProductDimensions).Include(x => x.Category)
                .Include(x => x.ProductCatalogs).Include(x => x.OwnedParts)
                .Include(x => x.ProductMaterials).ToList()
                .FirstOrDefault(x => x.Name == Name);
        }

        /// <summary>
        /// Get Products By Catalog Id
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        public IEnumerable<ProductReadDto> GetByCatalogId(int id)
        {
            return _context.Products
                .Include(x => x.Category)
                .Include(x => x.ProductDimensions)
                .Include(x => x.ProductCatalogs)
                .Include(x => x.ProductMaterials)
                .Include(x => x.OwnedParts)
                .Select(x => _assembler.ToDto(x)).ToList().FindAll(x => x.Catalogs.Contains(id));
        }
    }
}