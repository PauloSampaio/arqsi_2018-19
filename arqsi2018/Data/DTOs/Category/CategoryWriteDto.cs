using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Category
{
    public class CategoryWriteDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
    }
}