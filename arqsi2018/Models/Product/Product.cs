using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Restriction;

namespace arqsi2018.Models.Product
{
    public class Product
    {       
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ProductId { get; set; }
        private string _name;
        [Required]
        public string Name {get => _name;set => _name = CheckName(value);} 
        public string Description { get; set; }
        public string Reference { get; set; }
        public Category.Category Category { get; set; }
        //public double Price { get; set } 
         
        /// <summary>
        /// OwnedParts has the products (parts) whose parent is the current product.
        /// ParentProducts has the parent products which the current product is a part of.
        /// </summary>
        public virtual ICollection<Restriction.Restriction> OwnedParts { get; set; }
        public virtual ICollection<Restriction.Restriction> ParentProducts { get; set; }   
        
        public virtual ICollection<ProductCatalog> ProductCatalogs { get; set; }   
        public virtual ICollection<ProductMaterial> ProductMaterials { get; set; }  
        public virtual ICollection<ProductDimensions> ProductDimensions { get; set; }
        
        /// <summary>
        /// Checks if the name input is valid.
        /// Input must start with at least a character or number.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>string</returns>
        /// <exception cref="InvalidDataException"></exception>
        private string CheckName(string name)
        {
            var pattern = new Regex("^[0-9A-Za-z][0-9A-Za-z ]*");
            
            if (!pattern.IsMatch(name))
                throw new InvalidDataException(nameof(name));
            
            return name;          
        }

        /// <summary>
        /// Checks if the current Dimensions List fits in another Dimensions List
        /// To return true, at least 1 element of this list must fit in an element of the other list.
        /// </summary>
        /// <param name="productDimensions">ProductDimensions</param>
        /// <returns>bool</returns>
        public bool CheckFitsIn(ICollection<ProductDimensions> productDimensions)
        {   
            foreach (var thisDims in ProductDimensions)
            {
                foreach (var thatDims in productDimensions)
                {
                    if (thisDims.Dimensions.CheckFitsIn(thatDims.Dimensions))
                        return true;
                }
            }
            return false;
        }
            
        /// <summary>
        /// Checks if the current Dimensions List occupy at least a minimum target percentage of the elements of the other list.
        /// To return true, at least 1 element of this list must occupy the target percentage in an element of the other list.
        /// </summary>
        /// <param name="productDimensions"></param>
        /// <param name="minpct">int</param>
        /// <returns>bool</returns>
        public bool CheckFillsMinPct(ICollection<ProductDimensions> productDimensions, int minpct)
        {   
            foreach (var thisDims in ProductDimensions)
            {
                foreach (var thatDims in productDimensions)
                {
                    if (thisDims.Dimensions.CheckFillsMinPct(thatDims.Dimensions,minpct))
                        return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Checks if the current Dimensions List occupy at most a maximum target percentage of the elements of the other list.
        /// To return true, at least 1 element of this list mustn't occupy the target percentage in an element of the other list.
        /// </summary>
        /// <param name="productDimensions"></param>
        /// <param name="maxpct">int</param>
        /// <returns>bool</returns>
        public bool CheckFillsMaxPct(ICollection<ProductDimensions> productDimensions, int maxpct)
        {   
            foreach (var thisDims in ProductDimensions)
            {
                foreach (var thatDims in productDimensions)
                {
                    if (thisDims.Dimensions.CheckFillsMaxPct(thatDims.Dimensions,maxpct))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if the current Materials List match any material on the other list.
        /// </summary>
        /// <param name="productmaterials"></param>
        /// <returns>bool</returns>
        public bool CheckMaterialExists(ICollection<ProductMaterial> productmaterials)
        {
            foreach (var thisMat in ProductMaterials)
            {
                foreach (var thatMat in productmaterials)
                {
                    if (thisMat.MaterialId == thatMat.MaterialId)
                        return true;
                } 
            }
            return false;
        }

        public bool CheckValidRange(Dimensions.Dimensions dimensions)
        {
            foreach (var productDimension in ProductDimensions)
            {
                return productDimension.Dimensions.CheckValidRange(dimensions);
            }
            return false;
        }
    }
}