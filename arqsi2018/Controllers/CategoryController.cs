using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Category;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Controllers
{
    [Route("api/category")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryService _service;
        
        public CategoryController(ApiContext context)
        {
            _service = new CategoryService(new CategoryRepo(context));
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CategoryReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(CategoryReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }
        
        [HttpGet("name={name}")]
        [ProducesResponseType(200, Type = typeof(CategoryReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetByName(string name)
        {
            return Ok(_service.FindByName(name));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(CategoryReadDto))]
        public ActionResult Create([FromBody] CategoryWriteDto writeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CategoryReadDto result = _service.AddElement(writeDto);

            return Created($"api/categories/{result.Id}", result);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id, [FromBody] CategoryWriteDto writeDto)
        {
            CategoryReadDto result = _service.UpdateCategory(id, writeDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int id)
        {
            CategoryReadDto result = _service.DeleteCategory(id);
            return Ok(result);
        }
        
    }
}