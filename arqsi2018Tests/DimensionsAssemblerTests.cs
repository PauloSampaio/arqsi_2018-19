using System;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Models.Dimensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace arqsi2018Tests
{
    [TestClass]
    public class DimensionsAssemblerTests
    {
        private Dimensions _dimensions0,
            _dimensions1,
            _dimensions2,
            _dimensions3,
            _dimensions4,
            _dimensions5,
            _dimensions6,
            _dimensions7,
            _dimensions8,
            _dimensions9,
            _dimensions10,
            _dimensions11;

        private DimensionsReadDto _dimensionsReadDto;
        private DimensionsWriteDto _dimensionsWriteDto;
        private DimensionsAssembler _assembler;

        [TestCleanup]
        public void TestClean()
        {
            _dimensions0 = null;
            _dimensions1 = null;
            _dimensions2 = null;
            _dimensions3 = null;
            _dimensions4 = null;
            _dimensions5 = null;
            _dimensions6 = null;
            _dimensions7 = null;
            _dimensions8 = null;
            _dimensions9 = null;
            _dimensions10 = null;
            _dimensions11 = null;
            _dimensionsReadDto = null;
            _dimensionsWriteDto = null;
            _assembler = null;
        }

        [TestInitialize]
        public void TestInit()
        {
            //Create Dimensions
            _dimensions1 = DimensionsFactory.Create(10, 20, 10, 20, 10, 20);
            _dimensions2 = DimensionsFactory.Create(30, 40, 30, 40, 30, 40);
            _dimensions3 = DimensionsFactory.Create(50, 60, 50, 70, 50, 70);
            _dimensions4 = DimensionsFactory.Create(65, 65, 55, 55, 45, 45);
            
            _dimensions8 = DimensionsFactory.Create(65, 80, 65, 80, 65, 80);
            _dimensions11 = DimensionsFactory.Create(75, 100, 75, 100, 75, 100);
            
            _dimensions9 = DimensionsFactory.Create(90, 90, 90, 90, 90, 90);
            _dimensions10 = DimensionsFactory.Create(100, 100, 100, 100, 100, 100);
            

            //Create DimensionsWriteDto
            _dimensionsWriteDto = new DimensionsWriteDto()
            {
                LengthMin = 25, LengthMax = 45,
                WidthMin = 25, WidthMax = 45,
                HeightMin = 25, HeightMax = 45
            };

            //Create DimensionsAssembler
            _assembler = new DimensionsAssembler();
        }

        [TestMethod]
        public void ToDto()
        {
            _dimensionsReadDto = _assembler.ToDto(_dimensions1);

            Assert.AreEqual(_dimensionsReadDto.LengthMin, _dimensions1.LengthMin);
            Assert.AreEqual(_dimensionsReadDto.LengthMax, _dimensions1.LengthMax);

            Assert.AreEqual(_dimensionsReadDto.WidthMin, _dimensions1.WidthMin);
            Assert.AreEqual(_dimensionsReadDto.WidthMax, _dimensions1.WidthMax);

            Assert.AreEqual(_dimensionsReadDto.HeightMin, _dimensions1.HeightMin);
            Assert.AreEqual(_dimensionsReadDto.HeightMax, _dimensions1.HeightMax);
            
            Assert.AreEqual(_dimensionsReadDto.Id,_dimensions1.DimensionsId);
        }

        [TestMethod]
        public void ToDomainObject()
        {
            _dimensions0 = null;
            _dimensions0 = _assembler.ToDomainObject(_dimensionsWriteDto);

            Assert.AreEqual(_dimensions0.LengthMin, _dimensionsWriteDto.LengthMin);
            Assert.AreEqual(_dimensions0.LengthMax, _dimensionsWriteDto.LengthMax);

            Assert.AreEqual(_dimensions0.WidthMin, _dimensionsWriteDto.WidthMin);
            Assert.AreEqual(_dimensions0.WidthMax, _dimensionsWriteDto.WidthMax);

            Assert.AreEqual(_dimensions0.HeightMin, _dimensionsWriteDto.HeightMin);
            Assert.AreEqual(_dimensions0.HeightMax, _dimensionsWriteDto.HeightMax);
        }

        [TestMethod]
        public void CheckFitsIn()
        {
            Assert.IsTrue(_dimensions1.CheckFitsIn(_dimensions2));
            Assert.IsFalse(_dimensions2.CheckFitsIn(_dimensions1));
            Assert.IsTrue(_dimensions2.CheckFitsIn(_dimensions4));
            Assert.IsFalse(_dimensions4.CheckFitsIn(_dimensions2));
        }

        
        [TestMethod]
        public void CheckFillsAtLeast()
        {
            Assert.IsTrue(_dimensions2.CheckFillsAtLeast(_dimensions1));
            Assert.IsTrue(_dimensions1.CheckFillsAtLeast(_dimensions1));
            Assert.IsFalse(_dimensions2.CheckFillsAtLeast(_dimensions3));          
        }
        
        [TestMethod]
        public void CheckFillsMinPct()
        {
            Assert.IsTrue(_dimensions9.CheckFillsMinPct(_dimensions10,85));
            Assert.IsFalse(_dimensions9.CheckFillsMinPct(_dimensions10,95));
            Assert.IsTrue(_dimensions8.CheckFillsMinPct(_dimensions11,80));
            Assert.IsFalse(_dimensions9.CheckFillsMinPct(_dimensions10,95));
        }
        
        [TestMethod]
        public void CheckFillsMaxPct()
        {
            Assert.IsTrue(_dimensions9.CheckFillsMaxPct(_dimensions10,95));
            Assert.IsFalse(_dimensions9.CheckFillsMaxPct(_dimensions10,50));
            Assert.IsTrue(_dimensions8.CheckFillsMaxPct(_dimensions11,95));
            Assert.IsFalse(_dimensions9.CheckFillsMaxPct(_dimensions10,50));
        }
        
        
        [TestMethod]
        public void ConstructorValidations()
        {

            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => _dimensions5 = new Dimensions(0, 0, 0, 0, 0, 0)
            );

            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => _dimensions6 = new Dimensions(-1, -1, -1, -1, -1, -1)
            );

            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => _dimensions7 = new Dimensions(2, 1, 2, 1, 2, 1)
            );
        }

        [TestMethod]
        public void DimensionsSetter()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => _dimensions1.LengthMin = (_dimensions1.LengthMax+1)
            );
        }
    }
}