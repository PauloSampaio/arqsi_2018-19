using System;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.Repositories;
using arqsi2018.Models;
using arqsi2018.Models.Finish;

namespace arqsi2018.Data.Services
{
    public class FinishService
    {
        private readonly FinishRepo _repo;

        public FinishService(FinishRepo repo)
        {
            _repo = repo;
        }

        public IEnumerable<FinishReadDto> FindAll()
        {
            return _repo.GetAll();
        }

        public FinishReadDto AddElement(FinishWriteDto finishWriteDto)
        {
            Finish finish = _repo.Add(finishWriteDto);
            return _repo.GetById(finish.FinishId);
        }

        public FinishReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }

        public FinishReadDto UpdateFinish(int id, FinishWriteDto finishWriteDto)
        {
            return _repo.UpdateElement(id, finishWriteDto);
        }

        public FinishReadDto DeleteFinish(int id)
        {
            return _repo.DeleteElement(id);
        }

        public FinishReadDto FindByName(string Name)
        {
            return _repo.GetByName(Name);
        }
    }
}