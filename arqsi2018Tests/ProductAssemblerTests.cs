using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Models.Catalog;
using arqsi2018.Models.Category;
using arqsi2018.Models.Dimensions;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Material;
using arqsi2018.Models.Product;
using arqsi2018.Models.Restriction;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace arqsi2018Tests
{
    [TestClass]
    public class ProductAssemblerTests
    {
        private ProductAssembler _assembler;
        private Product _product1;
        private ProductWriteDto _writeDto;
        private ProductReadDto _readDto;
        private Material _material1, _material2;
        private Category _category1, _category2, _category3, _category4;
        private Dimensions _dimensions1, _dimensions2, _dimensions3, _dimensions4;
        private Catalog _catalog1, _catalog2, _catalog3;

        private ProductDimensions _productDimensions;
        private ProductCatalog _productCatalog;
        private ProductMaterial _productMaterial;
        private Restriction _ownedParts1, _ownedParts2;
        
        [TestCleanup]
        public void TestClean()
        {
            _category1 = null;_category2 = null;_category3 = null;_category4 = null;
            
            _dimensions1 = null;_dimensions2 = null;_dimensions3 = null;_dimensions4 = null;

            _material1 = null;_material2 = null;

            _catalog1 = null;_catalog2 = null;_catalog3 = null;

            _productCatalog = null;
            _productMaterial = null;
            _ownedParts1 = null;
            _ownedParts2 = null;

            _readDto = null;
            _writeDto = null;
            _product1 = null;

            _assembler = null;
        }

        [TestInitialize]
        public void TestInit()
        {
            _assembler = new ProductAssembler();
            
            _material1 = MaterialFactory.Create("Material_1", "Desc1");
            _material2 = MaterialFactory.Create("Material_2", "Desc2");
            _material1.MaterialId = 1;
            _material2.MaterialId = 2;
            
            _dimensions1 = DimensionsFactory.Create(10, 20, 10, 20, 10, 20);
            _dimensions2 = DimensionsFactory.Create(30, 40, 30, 40, 30, 40);
            _dimensions3 = DimensionsFactory.Create(50, 60, 50, 70, 50, 70);
            _dimensions4 = DimensionsFactory.Create(65, 65, 55, 55, 45, 45);
            _dimensions1.DimensionsId = 1;
            _dimensions2.DimensionsId = 2;
            _dimensions3.DimensionsId = 3;
            _dimensions4.DimensionsId = 4;
            
            _category1 = CategoryFactory.Create("Cat1", "Desc1", -1 , null);
            _category2 = CategoryFactory.Create("Cat2", "Desc2", -1 , null);
            _category3 = CategoryFactory.Create("Cat3", "Desc3", -1 , null);
            _category4 = CategoryFactory.Create("Cat4", "Desc4", -1 , null);
            _category1.CategoryId = 1;
            _category2.CategoryId = 2;
            _category3.CategoryId = 3;
            _category4.CategoryId = 4;

            _catalog1 = CatalogFactory.Create("Catalog1", "desc", new DateTime(2018, 1, 1));
            _catalog2 = CatalogFactory.Create("Catalog2", "desc", new DateTime(2019, 2, 1));
            _catalog3 = CatalogFactory.Create("Catalog3", "desc", new DateTime(2020, 3, 1));
            _catalog1.CatalogId = 1;
            _catalog2.CatalogId = 2;
            _catalog3.CatalogId = 3;

            _writeDto = new ProductWriteDto
            {
                Catalogs = new List<int>{1,2},
                CategoryId = 1,
                Description = "desc",
                Dimensionses = new List<int>{1,2},
                Materials = new List<int>{1,2},
                Name = "product",
                PartsIds = new List<int>(),
                Reference = "ref"
            };
        }

        [TestMethod]
        public void ToDomainObject()
        {
            _product1 = _assembler.ToDomainObject(_writeDto);
            
            Assert.AreEqual(_writeDto.Name, _product1.Name);
            Assert.AreEqual(_writeDto.Description, _product1.Description);
            Assert.AreEqual(_writeDto.Reference, _product1.Reference);
        }

        [TestMethod]
        public void ToDto()
        {
            _product1 = _assembler.ToDomainObject(_writeDto);
            _product1.ProductId = 1;

            
            
            
            _productDimensions = new ProductDimensions
            {
                Dimensions = _dimensions1,
                DimensionsId = _dimensions1.DimensionsId,
                Product = _product1,
                ProductId = _product1.ProductId
            };

            _product1.ProductMaterials = new List<ProductMaterial> {_productMaterial};
            _product1.ProductDimensions = new List<ProductDimensions> {_productDimensions};

            _product1.Category = _category1;

            _productCatalog = new ProductCatalog
            {
                Catalog = _catalog1,
                CatalogId = _catalog1.CatalogId,
                Product = _product1,
                ProductId = _product1.ProductId
            };

            _product1.ProductCatalogs = new List<ProductCatalog> {_productCatalog};

            _productMaterial = new ProductMaterial
            {
                Material = _material1,
                MaterialId = _material1.MaterialId,
                Product = _product1,
                ProductId = _product1.ProductId
            };

            _product1.ProductMaterials = new List<ProductMaterial> {_productMaterial};

            _product1.OwnedParts = new List<Restriction>();

            _readDto = _assembler.ToDto(_product1);

            Assert.AreEqual(_readDto.Name, _product1.Name);
            Assert.AreEqual(_readDto.Description, _product1.Description);
            Assert.AreEqual(_readDto.Reference, _product1.Reference);
            
            Assert.AreEqual(_readDto.Dimensionses.Count, _product1.ProductDimensions.Count);
            Assert.IsTrue(_readDto.Dimensionses.Contains(_dimensions1.DimensionsId));        

            Assert.AreEqual(_readDto.Catalogs.Count, _product1.ProductCatalogs.Count);
            Assert.IsTrue(_readDto.Catalogs.Contains(_catalog1.CatalogId));

            Assert.IsTrue(_readDto.Materials.Contains(_material1.MaterialId));
            Assert.IsTrue(!_readDto.PartsIds.Any());
        }

        [TestMethod]
        public void ToRestrictionDto()
        {
            _product1 = _assembler.ToDomainObject(_writeDto);
            _product1.ProductId = 1;
            var product2 = new Product{Name = "part"};
            product2.ProductId = 2;
            
            _ownedParts1 = new Restriction
            {
                
                Product = _product1,
                Part = product2,
                ProductId = _product1.ProductId,
                PartId = product2.ProductId
            };

            var readDto = _assembler.ToRestrictionDto(_ownedParts1);
            Assert.AreEqual(readDto.Name, _ownedParts1.Product.Name);
            Assert.AreEqual(readDto.ProductId, _ownedParts1.ProductId);
            Assert.AreEqual(readDto.PartId, _ownedParts1.PartId);
            Assert.AreEqual(readDto.PartName, _ownedParts1.Part.Name);
        }

        [TestMethod]
        public void ToParentsReadDto()
        {
            _product1 = _assembler.ToDomainObject(_writeDto);
            _product1.ProductId = 1;
            var product2 = new Product{Name = "part"};
            product2.ProductId = 2;
            var product3 = new Product{Name = "part"};
            product2.ProductId = 3;
            
            _ownedParts1 = new Restriction
            {
                Product =  product2,
                Part = _product1,
                ProductId = product2.ProductId,
                PartId = _product1.ProductId
            };
            
            _ownedParts2 = new Restriction
            {
                Product = product3,
                Part = _product1,
                ProductId = product3.ProductId,
                PartId = _product1.ProductId
            };
            
            _product1.ParentProducts = new List<Restriction>{_ownedParts1, _ownedParts2};

            var readDto = _assembler.ToParentsReadDto(_product1);
            Assert.AreEqual(readDto.Name, _product1.Name);
            Assert.AreEqual(readDto.ProductId, _product1.ProductId);
            Assert.AreEqual(readDto.ParentProducts.Count, 2);
        }

        [TestMethod]
        public void ToPartsDto()
        {
            _product1 = _assembler.ToDomainObject(_writeDto);
            _product1.ProductId = 1;
            var product2 = new Product{Name = "part"};
            product2.ProductId = 2;
            var product3 = new Product{Name = "part"};
            product2.ProductId = 3;
            
            _ownedParts1 = new Restriction
            {
                Product =  _product1,
                Part = product2,
                ProductId = _product1.ProductId,
                PartId = product2.ProductId
            };
            
            _ownedParts2 = new Restriction
            {
                Product = _product1,
                Part = product3,
                ProductId = _product1.ProductId,
                PartId = product3.ProductId
            };
            
            _product1.OwnedParts = new List<Restriction>{_ownedParts1, _ownedParts2};

            var readDto = _assembler.ToPartsDto(_product1);
            
            Assert.AreEqual(readDto.Name, _product1.Name);
            Assert.AreEqual(readDto.ProductId, _product1.ProductId);
            Assert.AreEqual(readDto.PartsIds.Count, _product1.OwnedParts.Count);
            Assert.IsTrue(readDto.PartsIds.Contains(product2.ProductId));
            Assert.IsTrue(readDto.PartsIds.Contains(product3.ProductId));


        }
    }
}