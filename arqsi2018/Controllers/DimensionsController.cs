using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Controllers
{
    [Route("api/dimensions")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class DimensionsController : ControllerBase
    {
        private readonly DimensionsService _service;

        public DimensionsController(ApiContext context)
        {          
            _service = new DimensionsService(new DimensionsRepo(context));         
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<DimensionsReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }        

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(DimensionsReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(DimensionsReadDto))]
        public ActionResult Create([FromBody] DimensionsWriteDto dimensionsWriteDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _service.AddElement(dimensionsWriteDto);

            return Created($"api/finishes/{result.Id}", result);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id,[FromBody] DimensionsWriteDto dimensionsWriteDto)
        {
            var result = _service.UpdateDimensions(id, dimensionsWriteDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id)
        {
            var result = _service.DeleteDimensions(id);
            return Ok(result);
        }



    }
}