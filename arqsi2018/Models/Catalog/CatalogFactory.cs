using System;
using System.Collections.Generic;
using arqsi2018.Models.Product;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.IdentityModel.Protocols.WsFederation;

namespace arqsi2018.Models.Catalog
{
    public class CatalogFactory
    {
        public static Catalog Create(
            string name, string description, DateTime date)
        {
            return new Catalog
            {
                Name = name,
                Description = description,
                Date = date,
            };
        }
    }
}