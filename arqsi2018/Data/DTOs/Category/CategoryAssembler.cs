using System;
using System.Collections.Generic;
using arqsi2018.Models.Category;

namespace arqsi2018.Data.DTOs.Category
{
    public class CategoryAssembler : AssemblerBase<CategoryReadDto, CategoryWriteDto, Models.Category.Category>
    {
        public CategoryReadDto ToDto(Models.Category.Category category)
        {
            if(category == null)
                return new CategoryReadDto();
            
            return new CategoryReadDto
            {
                Id = category.CategoryId,
                Name = category.Name,
                Description = category.Description,
                ParentId = category.ParentId,
                Children = new List<int>()
            };

        }

        public Models.Category.Category ToDomainObject(CategoryWriteDto dto)
        {
            
            if (ValidateStrings(dto))
            {
                return new Models.Category.Category
                {
                    Name = dto.Name,
                    Description = dto.Description,
                    ParentId = dto.ParentId
                };
            }
            return null;
        }

        private static bool ValidateStrings(CategoryWriteDto dto)
        {
            return (!string.IsNullOrEmpty(dto.Name)) && (!string.IsNullOrEmpty(dto.Description));
        }
    }
}