using System;
using System.Collections.Generic;
using System.IO;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Category;
using arqsi2018.Models.Category;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace arqsi2018Tests
{
    [TestClass]
    public class CategoryAssemblerTests
    {
        private Category _category1, _category2, _category3, _category4;
        private CategoryReadDto _categoryReadDto2, _categoryReadDto4;
        private CategoryWriteDto _categoryWriteDto1, _categoryWriteDto2, _categoryWriteDto4;
        private string _str1, _str2, _str3;
        private CategoryAssembler _assembler;

        [TestCleanup]
        public void TestClean()
        {
            _str1 = null;
            _str2 = null;
            _str3 = null; 
            
            _category1 = null;
            _category2 = null;
            _category3 = null;
            _category4 = null;

            _categoryReadDto2 = null;
            _categoryReadDto4 = null;

            _categoryWriteDto1 = null;
            _categoryWriteDto2 = null;
            _categoryWriteDto4 = null;
            
            _assembler = null;
        }

        [TestInitialize]
        public void TestInit()
        {    
            //Create Strings
            _str1 = "Valid Name Test 12345";
            _str2 = "";
            _str3 = " Invalid Start";
            
            _category1 = CategoryFactory.Create("Cat1", "Desc1", -1 ,null );
            _category2 = CategoryFactory.Create("Cat2", "Desc2", -1 , null);
            _category3 = CategoryFactory.Create("Cat3", "Desc3", -1 , null);
            _category4 = CategoryFactory.Create("Cat4", "Desc4", -1 , null);
            
            _categoryReadDto2 = new CategoryReadDto
            {
                Name = "Cat2", 
                Description = "Desc2",
            };
            _categoryReadDto4 = new CategoryReadDto
            {
                Name = "Cat4",
                Description = "Desc4",
            };
            
            _categoryWriteDto2 = new CategoryWriteDto
            {
                Name = _category2.Name,
                Description = _category2.Description,
            };
            _categoryWriteDto4 = new CategoryWriteDto
            {
                Name = _category4.Name,
                Description = _category4.Description,
            };
            
            _categoryWriteDto1 = new CategoryWriteDto
            {
                Name = _category1.Name,
                Description = _category1.Description,
                ParentId = -1,
            };
            
            _assembler = new CategoryAssembler();
        }

        [TestMethod]
        public void ToDto()
        {
            var categoryReadDto = _assembler.ToDto(_category1);
            
            Assert.AreEqual(categoryReadDto.Name, _category1.Name);
            Assert.AreEqual(categoryReadDto.Description, _category1.Description);
            Assert.AreEqual(categoryReadDto.ParentId, _category1.ParentId);
        }

       [TestMethod]
        public void ToDomainObject()
        {
            var newCategory = CategoryFactory.Create(_categoryWriteDto1.Name, _categoryWriteDto1.Description, _categoryWriteDto1.ParentId, null);
            Assert.AreEqual(newCategory.Name, _category1.Name);
            Assert.AreEqual(newCategory.Description, _category1.Description);
            Assert.AreEqual(newCategory.ParentId, _category1.ParentId);
        }
        
        [TestMethod]
        public void NameChecking()
        {
            _category1.Name = _str1;
           
            Assert.ThrowsException<InvalidDataException>(
                () => _category1.Name = _str2
            );
       
            Assert.ThrowsException<InvalidDataException>(
                () => _category1.Name = _str3
            );
        }
        
        [TestMethod]
        public void CheckParent()
        {
            _category2.CategoryId = -1;
            _category2.ParentId = -1;
            
            _category1.ParentId = -1;
            _category1.CategoryId = 1;
           
            Assert.ThrowsException<InvalidDataException>(
                () => _category1.ParentId = 1
            );


        }
    }
}