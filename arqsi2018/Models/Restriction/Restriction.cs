using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace arqsi2018.Models.Restriction
{
    /// <summary>
    /// Restriction 
    /// </summary>
    public class Restriction
    {   
        public int ProductId { get; set; }
        public Product.Product Product { get; set; }
        public int PartId { get; set; }
        public Product.Product Part { get; set; }
        
        public bool IsRequired { get; set; }
        public bool UseBaseProductMaterial { get; set; }
        
        private int _minDimensionsPct;
        public int MinDimensions {get => _minDimensionsPct;set => _minDimensionsPct = CheckPercentage(value);}

        private int _maxDimensionsPct;
        public int MaxDimensions {get => _maxDimensionsPct;set => _maxDimensionsPct = CheckPercentage(value);}
        
        /// <summary>
        /// Check Percentage, verifies whether the input int is a valid percentage
        /// We may want a part to fill "at least" a min of x% of the base product (but never 100%)
        /// Or we may want it to fill "at most" a max of x% of the base product (but never 0%)
        /// 0 is allowed in order to mark this restriction as not being used.
        /// </summary>
        /// <param name="pct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private int CheckPercentage(int pct)
        {
            if ((pct < 100) && (pct >= 0))
                return pct;

            throw new ArgumentOutOfRangeException(nameof(pct));
        }

        /// <summary>
        /// Checks if any material of the current part match any material on the base product
        /// </summary>
        /// <returns>bool</returns>
        public bool CheckMaterialExists()
        {
            return Product.CheckMaterialExists(Part.ProductMaterials);
        }

        /// <summary>
        /// Validates whether the sum of mandatory min% is under 100%
        /// </summary>
        /// <param name="pct">int</param>
        /// <returns>bool</returns>
        public bool CheckMinOccupation(int pct)
        {        
            foreach (var ownedPart in Product.OwnedParts)
            {
                if(ownedPart.IsRequired)
                    pct += ownedPart._minDimensionsPct;
            }
            return pct < 100;
        }
        
        /// <summary>
        /// Validates whether the sum of mandatory max% is under 100%
        /// </summary>
        /// <param name="pct">int</param>
        /// <returns>bool</returns>
        public bool CheckMaxOccupation(int pct)
        {            
            foreach (var ownedPart in Product.OwnedParts)
            {
                if(ownedPart.IsRequired)
                    pct += ownedPart._maxDimensionsPct;
            }
            return pct < 100;
        }

        /// <summary>
        /// Runs all restriction validation strategies with the current restriction
        /// </summary>
        /// <returns>bool</returns>
        public bool ValidateRestrictions()
        {
            var restrictionstrategies = new CompositeRestrictionStrategy();
            restrictionstrategies.Add(new RestrictionByMaterialStrategy());
            restrictionstrategies.Add(new RestrictionByPercentageStrategy());
            restrictionstrategies.Add(new RestrictionByRequiredStrategy());

            return restrictionstrategies.ValidateRestrictions(this);
        }
    }
}