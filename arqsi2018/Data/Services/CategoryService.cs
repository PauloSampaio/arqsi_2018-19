using System;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Category;
using arqsi2018.Data.Repositories;

namespace arqsi2018.Data.Services
{
    public class CategoryService
    {
        private readonly CategoryRepo _repo;

        public CategoryService(CategoryRepo repo)
        {
            _repo = repo;
        }

        public IEnumerable<CategoryReadDto> FindAll()
        {
            return _repo.GetAll();
        }

        public CategoryReadDto AddElement(CategoryWriteDto writeDto)
        {
            var category = _repo.Add(writeDto);
            return _repo.GetById(category.CategoryId);
        }

        public CategoryReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }

        public CategoryReadDto UpdateCategory(int id, CategoryWriteDto writeDto)
        {
            return _repo.UpdateElement(id, writeDto);
        }

        public CategoryReadDto DeleteCategory(int id)
        {
            return _repo.DeleteElement(id);
        }

        public CategoryReadDto FindByName(string Name)
        {
            return _repo.GetByName(Name);
        }
    }
}