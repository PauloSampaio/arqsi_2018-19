using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using arqsi2018.Controllers;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;
using arqsi2018.Models.Dimensions;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Product;
using arqsi2018.Models.Restriction;

namespace arqsi2018.Api.Data
{
    public class ApiRepo
    {
        private readonly ApiContext _context;
        private readonly ProductAssembler _assembler;
        private readonly ProductRepo _repo;
        private readonly MaterialRepo _mrepo;
        private readonly DimensionsRepo _drepo;
        
        public ApiRepo(ApiContext context)
        {
            _context = context;
            _assembler = new ProductAssembler();
            _repo = new ProductRepo(context);
            _mrepo = new MaterialRepo(context);
            _drepo = new DimensionsRepo(context);
        }

        public int ValidateItems(ItemsList itemsList)
        {
            
            foreach (var item in itemsList.items)
            {
                if (!ValidateItemProduct(item))
                    return item.ProductId;
            }

            
           
            // validate is required restriction
            var mainitem = itemsList.items.FirstOrDefault();
            var restrictionslist = _repo.GetByIdRestrictions(mainitem.ProductId);

            foreach (var restriction in restrictionslist)
            {
                if (restriction.IsRequired)
                {

                   var thelist = itemsList.items.ToList().Where(x => x.ProductId == restriction.PartId);

                    if (thelist.ToList().Count == 0)
                        return mainitem.ProductId;
                }
            }


            return -1;
        }
            

        private bool ValidateItemProduct(ItemWriteDto item)
        {
            var product = _repo.GetProductById(item.ProductId);
            var material = _mrepo.GetById(item.MaterialId);
            var dimensions = ValidateConcreteDimensions(item);

            // invalid dimensions inserted
            if (dimensions == null)
                return false;
            
            // non existent id 
            if (product == null)
                return false;
            
            // non existent material id
            if (material.Id == 0)
                return false;

            // incoherent finish id
            if (!material.Finishes.Contains(item.FinishId))
                return false;
            
            // invalid if the inserted dimensions values are outside the product's dimensions range
            if (!product.CheckValidRange(dimensions))
                return false;

            // does parent and restriction related validations
            if (!ValidateParent(item))
                return false;
               
            return true;
        }

        /// <summary>
        /// aux method to do parent and restriction validations
        /// </summary>
        /// <param name="item">ItemWriteDto</param>
        /// <returns>bool</returns>
        private bool ValidateParent(ItemWriteDto item)
        {
            var dimensions = ValidateConcreteDimensions(item); // just to get a dimensions object
            
            // parent related validations
            if (item.ParentId != 0)
            {
                var parentdto = _repo.GetById(item.ParentId);

                // non existent parent id
                if (parentdto == null)
                    return false;
           
                // validate if parent is this part's parent
                if (!parentdto.PartsIds.Contains(item.ProductId))
                    return false;
                
                var restrictiondto = _repo.GetByCompKeyRestriction(item.ParentId, item.ProductId);
                
                // material based restriction validation
                if (restrictiondto.UseBaseProductMaterial)
                {
                    if (!parentdto.Materials.Contains(item.MaterialId))
                        return false;
                }

                // percentage based restriction validation (checks whether the concrete dimensions
                // are according to the min\max % restriction if any exists
                
                var product = _repo.GetProductById(item.ProductId);
                var parent = _repo.GetProductById(item.ParentId);

                product.ProductDimensions = new List<ProductDimensions>();
                var partialPd = new ProductDimensions();
                partialPd.Dimensions = dimensions;
                product.ProductDimensions.Add(partialPd);

                if(restrictiondto.MaxDimensions > 0)
                    if (!product.CheckFillsMaxPct(parent.ProductDimensions, restrictiondto.MaxDimensions))
                        return false;
                    
                if(restrictiondto.MinDimensions > 0)
                    if (!product.CheckFillsMinPct(parent.ProductDimensions, restrictiondto.MinDimensions))
                        return false;
            }

            return true;
        }
        
        /// <summary>
        /// aux method to validate if the dimesnions are well formed
        /// </summary>
        /// <param name="item">ItemWriteDto</param>
        /// <returns>dimensions</returns>
        private Dimensions ValidateConcreteDimensions(ItemWriteDto item)
        {
            // will throw out of range exception if invalid values are inserted
            try
            {
                var dimensions = DimensionsFactory
                    .Create(item.Length, item.Length, item.Width, 
                        item.Width, item.Height, item.Height);

                return dimensions;
            }
            catch (Exception e)
            {
                return null;
            } 
        } 
    }
}