
using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Controllers
{
    [Route("api/product")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductService _service;

        public ProductController(ApiContext context)
        {
            _service = new ProductService(new ProductRepo(context));
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ProductReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(ProductReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }
        
        [HttpGet("{id}/parts")]
        [ProducesResponseType(200, Type = typeof(ProductPartsReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetPartsById(int id)
        {
            return Ok(_service.FindPartsById(id));
        }
        
        [HttpGet("{id}/partof")]
        [ProducesResponseType(200, Type = typeof(ProductParentsReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetPartOfById(int id)
        {
            return Ok(_service.FindParentsById(id));
        }
        
        [HttpGet("{id}/restrictions")]
        [ProducesResponseType(200, Type = typeof(RestrictionReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetRestrictionsById(int id)
        {
            return Ok(_service.FindRestrictionsById(id));
        }
        
        [HttpGet("name={name}")]
        [ProducesResponseType(200, Type = typeof(ProductReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetByName(string name)
        {
            return Ok(_service.FindByName(name));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(ProductReadDto))]
        public ActionResult Create([FromBody] ProductWriteDto writeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ProductReadDto result = _service.AddElement(writeDto);
            
            if (result.PartsIds.Count != writeDto.PartsIds.Count)
            {
                string str= "\nAdded Parts: ";
                foreach (var partid in result.PartsIds)
                {
                    str = str + "partid:" + partid;
                }
                return BadRequest("ERROR: Not all parts could be added to "+result.Name+"."+str);
            }

            return Created($"api/products/{result.ProductId}", result);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id, [FromBody] ProductWriteDto writeDto)
        {
            ProductReadDto result = _service.UpdateProduct(id, writeDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int id)
        {
            ProductReadDto result = _service.DeleteProduct(id);
            return Ok(result);
        }
        
        [HttpGet("catalog={catalogid}")]
        [ProducesResponseType(200, Type = typeof(ProductParentsReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetProductByCatalogId(int catalogid)
        {
            return Ok(_service.FindByCatalogId(catalogid));
        }
    }
}