using System.Collections.Generic;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;

namespace arqsi2018.Data.Services
{
    public class ProductService
    {
        private readonly ProductRepo _repo;

        public ProductService(ProductRepo repo)
        {
            _repo = repo;
        }

        public IEnumerable<ProductReadDto> FindAll()
        {
            return _repo.GetAll();
        }
        
        public IEnumerable<RestrictionReadDto> FindAllRestrictions()
        {
            return _repo.GetAllRestrictions();
        }

        public ProductReadDto AddElement(ProductWriteDto writeDto)
        {
            var product = _repo.Add(writeDto);
            
            if (product == null)
                return null;
            else
                return _repo.GetById(product.ProductId);
        }

        public ProductReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }
        
        public ProductPartsReadDto FindPartsById(int id)
        {
            return _repo.GetByIdParts(id);
        }
        
        public ProductParentsReadDto FindParentsById(int id)
        {
            return _repo.GetByIdPartOf(id);
        }
        
        public IEnumerable<RestrictionReadDto> FindRestrictionsById(int id)
        {
            return _repo.GetByIdRestrictions(id);
        }
        
        public RestrictionReadDto FindRestrictionByCompKey(int id,int idpart)
        {
            return _repo.GetByCompKeyRestriction(id, idpart);
        }

        public ProductReadDto UpdateProduct(int id, ProductWriteDto writeDto)
        {
            return _repo.UpdateElement(id, writeDto);
        }

        public ProductReadDto DeleteProduct(int id)
        {
            return _repo.DeleteElement(id);
        }
        
        public ProductReadDto FindByName(string Name)
        {
            return _repo.GetByName(Name);
        }
        
        public IEnumerable<ProductReadDto> FindByCatalogId(int id)
        {
            return _repo.GetByCatalogId(id);
        }
    }
}