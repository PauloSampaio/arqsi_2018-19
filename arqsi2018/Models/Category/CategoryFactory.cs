using System.Collections.Generic;

namespace arqsi2018.Models.Category
{
    public class CategoryFactory
    {
        public static Category Create(
            string name, string description, int? parentId, Category parent)
        {          
            return new Category
            {
                Name = name,
                Description = description,
                ParentId = parentId,
                Parent = parent,
            };
        }
    }
}