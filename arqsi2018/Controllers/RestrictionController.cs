using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Controllers
{
    [Route("api/restriction")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class RestrictionController : ControllerBase
    {
        private readonly ProductService _service;
        
        public RestrictionController(ApiContext context)
        {
            _service = new ProductService(new ProductRepo(context));
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<RestrictionReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAllRestrictions());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(RestrictionReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetRestrictionsById(int id)
        {
            return Ok(_service.FindRestrictionsById(id));
        }
        
        [HttpGet("{id}/{idpart}")]
        [ProducesResponseType(200, Type = typeof(RestrictionReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetRestrictionsByCompKey(int id, int idpart)
        {
            return Ok(_service.FindRestrictionByCompKey(id, idpart));
        }
    }
}