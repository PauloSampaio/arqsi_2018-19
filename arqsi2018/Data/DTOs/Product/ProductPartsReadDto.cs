using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Product
{
    public class ProductPartsReadDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        
        public ICollection<int> PartsIds { get; set; }
    }
}