namespace arqsi2018.Data.DTOs.Finish
{
    public class FinishWriteDto
    {   
        public string Name { get; set; }
        public string Description { get; set; }
    }
}