using System;
using arqsi2018.Models;
using arqsi2018.Models.Finish;

namespace arqsi2018.Data.DTOs.Finish
{
    public class FinishAssembler : AssemblerBase<FinishReadDto, FinishWriteDto, Models.Finish.Finish>
    {
        public FinishReadDto ToDto(Models.Finish.Finish finish)
        {
            if(finish == null)
                return new FinishReadDto();
            
            FinishReadDto finishReadDto = new FinishReadDto();
            finishReadDto.Name = finish.Name;
            finishReadDto.Id = finish.FinishId;
            finishReadDto.Description = finish.Description;

            return finishReadDto;
        }

        public Models.Finish.Finish ToDomainObject(FinishWriteDto dto)
        {
            return FinishFactory.Create(dto.Name, dto.Description);
        }
    }
}