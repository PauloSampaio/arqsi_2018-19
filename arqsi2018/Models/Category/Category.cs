using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace arqsi2018.Models.Category
{
    public class Category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CategoryId { get; set; }

        private string _name;
        private int? _parentId;
        
        [Required]
        public string Name {get => _name;set => _name = CheckName(value);}
        public string Description { get; set; }
        [ForeignKey("Category")]
        public int? ParentId { get => _parentId; set => _parentId = CheckParent(value); }
        public virtual Category Parent { get; set; } 
        
        /// <summary>
        /// Checks if the name input is valid.
        /// Input must start with at least a character or number.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>string</returns>
        /// <exception cref="InvalidDataException"></exception>
        private string CheckName(string name)
        {
            var pattern = new Regex("^[0-9A-Za-z][0-9A-Za-z ]*");
            
            if (!pattern.IsMatch(name))
                throw new InvalidDataException(nameof(name));
            
            return name;          
        }
        
        /// <summary>
        /// Ensures that the parent is never set to self unless it's the root.
        /// </summary>
        /// <param name="parentId">int</param>
        /// <returns>int</returns>
        /// <exception cref="InvalidDataException"></exception>
        private int? CheckParent(int? parentId)
        {
            if (parentId == 1)
                return 1;
            
            if (parentId == CategoryId)
                throw new InvalidDataException(nameof(parentId));
     
            return parentId;          
        }
    }
}