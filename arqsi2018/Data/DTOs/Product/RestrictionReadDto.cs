namespace arqsi2018.Data.DTOs.Product
{
    public class RestrictionReadDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int PartId { get; set; }
        public string PartName { get; set; }
        public bool IsRequired { get; set; }
        public bool UseBaseProductMaterial { get; set; }
        public int MinDimensions { get; set; }
        public int MaxDimensions { get; set; }
    }
}