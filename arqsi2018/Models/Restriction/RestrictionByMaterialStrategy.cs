namespace arqsi2018.Models.Restriction
{
    public class RestrictionByMaterialStrategy : IRestrictionStrategy
    {
        public bool ValidateRestrictions(Restriction restriction)
        {
            if(restriction.UseBaseProductMaterial)
                return restriction.CheckMaterialExists();

            return true;
        }
    }
}