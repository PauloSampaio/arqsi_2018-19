using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Product
{
    public class ProductReadDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }    
        public int CategoryId { get; set; }
           
        public ICollection<int> PartsIds { get; set; }
        public ICollection<int> Catalogs { get; set; }
        public ICollection<int> Materials { get; set; }
        public ICollection<int> Dimensionses { get; set; }
    }
}