using System;

namespace arqsi2018.Data.DTOs.Dimensions
{
    public class DimensionsReadDto
    {
        public int Id { get; set; }
        public double LengthMin { get; set; }
        public double LengthMax { get; set; }
        
        public double WidthMin { get; set; }
        public double WidthMax { get; set; }
        
        public double HeightMin { get; set; }
        public double HeightMax { get; set; }       
    }
}