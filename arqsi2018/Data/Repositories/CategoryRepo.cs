using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.Xml;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Category;
using arqsi2018.Models.Category;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.IdentityModel.Xml;
using Remotion.Linq.Clauses;

namespace arqsi2018.Data.Repositories
{
    public class CategoryRepo :RepoBase<CategoryReadDto, CategoryWriteDto, Category>
    {
        private readonly ApiContext _context;
        private readonly CategoryAssembler _assembler;
        
        public CategoryRepo(ApiContext context)
        {
            _context = context;
            _assembler = new CategoryAssembler();
        }
        
        /**
         *
         ***************************************
         *            GET_ALL                  *
         ***************************************
         * 
         */
        public IEnumerable<CategoryReadDto> GetAll()
        {

            var categories = _context.Categories.Where(x => x.CategoryId != 1).ToList();
            var categoriesDtoList = new List<CategoryReadDto>();
            
            foreach (var x in categories)
            {
                var dto = _assembler.ToDto(x);
                dto.Children = new List<int>();

                var result = from child in _context.Categories
                    where (child.CategoryId != 1) && (child.ParentId == dto.Id)
                    select child;
                    
                foreach (var y in result)
                    dto.Children.Add(y.CategoryId);
    
                categoriesDtoList.Add(dto);
            }
            return
                categoriesDtoList;
        }

        /**
         *
         ***************************************
         *            POST                     *
         ***************************************
         * 
         */
        
        public Category Add(CategoryWriteDto writeDto)
        {
            var parent = GetCategoryById(writeDto.ParentId);
            
            //forces a root element if none exists
            if (parent == null)
            {
                if(!RootExistCheck())
                {
                    CreateRoot(); 
                }
                parent = GetCategoryById(1);
            }

            var category = _assembler.ToDomainObject(writeDto);
            category.Parent = parent;
            
            var result = _context.Add(category).Entity;
            _context.SaveChanges();
            return result;
        }

        /**
         *
         ***************************************
         *            GET_BY_ID                *
         ***************************************
         * 
         */
        
        public CategoryReadDto GetById(int id)
        {
            var category = GetCategoryById(id);
            var categoryDto = new CategoryReadDto();
                
            if (category == null)
                return categoryDto;
            
            categoryDto = _assembler.ToDto(category);
            categoryDto.Children = new List<int>();
            
            var result = from child in _context.Categories
                where (child.CategoryId != 1) && (child.ParentId == categoryDto.Id)
                select child;
            
            foreach (var y in result)
                categoryDto.Children.Add(y.CategoryId);
 
            return categoryDto;
        }
        
        /**
         *
         ***************************************
         *            GET_BY_NAME              *
         ***************************************
         * 
         */
        
        public CategoryReadDto GetByName(string Name)
        {
            var category = GetCategoryByName(Name);
            var categoryDto = new CategoryReadDto();
                
            if (category == null)
                return categoryDto;
            
            categoryDto = _assembler.ToDto(category);
            categoryDto.Children = new List<int>();
            
            var result = from child in _context.Categories
                where (child.CategoryId != 1) && (child.ParentId == categoryDto.Id)
                select child;
            
            foreach (var y in result)
                categoryDto.Children.Add(y.CategoryId);
 
            return categoryDto;
        }
        
        
        
        /**
         *
         ***************************************
         *                PUT                  *
         ***************************************
         * 
         */
        
        public CategoryReadDto UpdateElement(int id, CategoryWriteDto writeDto)
        {
            var toUpdate = GetCategoryById(id);

            if(writeDto.Name != null){toUpdate.Name = writeDto.Name;}
            if(writeDto.Description != null){toUpdate.Description = writeDto.Description;}

            var parent = GetCategoryById(writeDto.ParentId);

            //if parent is null, it makes parent id = -1
            toUpdate.Parent = parent ?? GetCategoryById(1);

            _context.SaveChanges();
            return GetById(id);
        }     

        /**
         *
         ***************************************
         *                DEL                  *
         ***************************************
         * 
         */
        public CategoryReadDto DeleteElement(int id)
        {
            if(id == 1)
                return new CategoryReadDto();
            
            var toDelete = GetCategoryById(id);
            _context.Remove(toDelete);
            _context.SaveChanges();
            return _assembler.ToDto(toDelete);
        }
        
        private Category GetCategoryById(int? id)
        {
            return _context.Categories.Find(id);
        }
        
        /// <summary>
        /// Verifica se existe elemento raíz. Ou seja se o GetAll retorna alguma coisa.
        /// </summary>
        /// <returns>bool</returns>
        private bool RootExistCheck()
        {
            return _context.Categories.Any();
            //return GetAll().Any();
        }

        /// <summary>
        /// Creates the Root Category if none exists
        /// </summary>
        private void CreateRoot()
        {
            var root = CategoryFactory.Create("Root", "Root",null, null);
            //root.CategoryId = -1;
            _context.Categories.Add(root);
            _context.SaveChanges();
        }
        
        private Category GetCategoryByName(string Name)
        {
            return _context.Categories.ToList()
                .FirstOrDefault(x => x.Name == Name);
        }
    }
}