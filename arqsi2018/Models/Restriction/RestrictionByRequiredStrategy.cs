namespace arqsi2018.Models.Restriction
{
    public class RestrictionByRequiredStrategy : IRestrictionStrategy
    {
        public bool ValidateRestrictions(Restriction restriction)
        {
            if (restriction.IsRequired)
            {
                // if the sum of mandatory min occupation% is >= 100% return false
                if (!restriction.CheckMinOccupation(restriction.MinDimensions))
                    return false;
                
                // if the sum of mandatory max occupation% is >= 100% return false
                if (!restriction.CheckMaxOccupation(restriction.MaxDimensions))
                    return false;
            }
            return true;
        }
    }
}