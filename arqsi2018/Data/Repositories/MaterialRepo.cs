using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.Services;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Material;
using arqsi2018.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace arqsi2018.Data.Repositories
{
    public class MaterialRepo : RepoBase<MaterialReadDto,MaterialWriteDto,Material>
    {
        private readonly ApiContext _context;
        private readonly MaterialAssembler _assembler;
        public MaterialRepo(ApiContext context)
        {
            _context = context;
            _assembler = new MaterialAssembler();
        }
        
        /**
         *
         ***************************************
         *            GET_ALL                  *
         ***************************************
         * 
         */
        public IEnumerable<MaterialReadDto> GetAll()
        {
            return _context.Materials.Include(x => x.MaterialFinishes).Select(x =>_assembler.ToDto(x)).ToList();
        }

        /**
         *
         ***************************************
         *            POST                     *
         ***************************************
         * 
         */
        public Material Add(MaterialWriteDto writeDto)
        {
            var material = _assembler.ToDomainObject(writeDto);
            material.MaterialFinishes = new List<MaterialFinish>();
            var result = _context.Add(material).Entity;
            _context.SaveChanges();

            if (writeDto.Finishes == null) return result;
            
            result.MaterialFinishes = GetMaterialFinishes(result, writeDto);
            _context.SaveChanges();

            return result;
        }

        /**
         *
         ***************************************
         *            GET_BY_ID                *
         ***************************************
         * 
         */
        public MaterialReadDto GetById(int id)
        {
            var material = GetMaterialById(id);
           
            //Todo: Better not found handling
            return material == null ? new MaterialReadDto() : _assembler.ToDto(material);
        }
        
        /**
         *
         ***************************************
         *            GET_BY_NAME               *
         ***************************************
         * 
         */
        public MaterialReadDto GetByName(string Name)
        {
            return _assembler.ToDto(GetMaterialByName(Name));
        }

        /**
         *
         ***************************************
         *                PUT                  *
         ***************************************
         * 
         */
        public MaterialReadDto UpdateElement(int id, MaterialWriteDto writeDto)
        {
            var material = GetMaterialById(id);

            if (writeDto.Name != null)
            {
                material.Name = writeDto.Name;
            }

            if (writeDto.Description != null)
            {
                material.Description = writeDto.Description;
            }
            
            if (writeDto.Finishes == null)
            {
                _context.SaveChanges();
                return GetById(id); 
            }
            material.MaterialFinishes = GetMaterialFinishes(material, writeDto);
 
            _context.SaveChanges();

            return GetById(id);
        }
        
        /**
         *
         ***************************************
         *                DEL                  *
         ***************************************
         * 
         */
        public MaterialReadDto DeleteElement(int id)
        {
            var toDelete = GetMaterialById(id);
            _context.Remove(toDelete);
            _context.SaveChanges();
            return _assembler.ToDto(toDelete);
        }
        
        private ICollection<MaterialFinish> GetMaterialFinishes(Material material, MaterialWriteDto writeDto)
        {
            material.MaterialFinishes = new List<MaterialFinish>();
            _context.SaveChanges();
            
            var materialFinishes = new List<MaterialFinish>();

            foreach (var id in writeDto.Finishes)
            {
                var finish = _context.Finishes.Find(id);
                var materialFinish = _context.MaterialFinishes.Add(
                    new MaterialFinish
                    {
                        Material = material,
                        Finish = finish,
                        FinishId = finish.FinishId,
                        MaterialId = material.MaterialId
                    }
                ).Entity;
                _context.SaveChanges();
                    
                materialFinishes.Add(materialFinish);
            }

            return materialFinishes;
        }
        
        /// <summary>
        /// Get Material By Id - private aux method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Material GetMaterialById(int id)
        {
            return _context.Materials.Include(x => x.MaterialFinishes).ToList().FirstOrDefault(x => x.MaterialId == id);
            //return _context.Materials.Find(id); //just for reference, no longer used
        }
        
        private Material GetMaterialByName(string name)
        {
            return _context.Materials
                .Include(x => x.MaterialFinishes).ToList()
                .FirstOrDefault(x => x.Name == name);
        }
    }
}