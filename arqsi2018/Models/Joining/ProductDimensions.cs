namespace arqsi2018.Models.Joining
{
    public class ProductDimensions
    {
        public int ProductId { get; set; }
        public Product.Product Product { get; set; }
        public int DimensionsId { get; set; }
        public Dimensions.Dimensions Dimensions { get; set; }
    }
}