using System.Collections.Generic;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.Repositories;
using arqsi2018.Models.Dimensions;

namespace arqsi2018.Data.Services
{
    public class DimensionsService
    {
        private readonly DimensionsRepo _repo;
        
        public DimensionsService(DimensionsRepo repo)
        {
            _repo = repo;
        }
        
        public IEnumerable<DimensionsReadDto> FindAll()
        {
            return _repo.GetAll();
        }

        public DimensionsReadDto AddElement(DimensionsWriteDto dimensionsWriteDto)
        {
            var dimensions = _repo.Add(dimensionsWriteDto);
            return _repo.GetById(dimensions.DimensionsId);
        }

        public DimensionsReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }

        public DimensionsReadDto UpdateDimensions(int id, DimensionsWriteDto dimensionsWriteDto)
        {
            return _repo.UpdateElement(id, dimensionsWriteDto);
        }

        public DimensionsReadDto DeleteDimensions(int id)
        {
            return _repo.DeleteElement(id);
        }
        
        
    }
}