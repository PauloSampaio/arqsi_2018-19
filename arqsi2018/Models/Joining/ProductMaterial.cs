namespace arqsi2018.Models.Product
{
    public class ProductMaterial
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int MaterialId { get; set; }
        public Material.Material Material { get; set; }
    }
}