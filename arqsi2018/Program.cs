﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace arqsi2018
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            
            /*
            // PARA APAGAR MAIS TARDE, SERVE APENAS DE EXEMPLO
            // ESTES MÉTODOS REFEREM-SE ÀS CLASSES DO PACKAGE TESTJASON
            SerializationRepository.OutpuJson();
            
            SerializationRepository.DeserializeJson();
            
            SerializationRepository.SerializeJson();
            
            SerializationRepository.SerializedJsonIndented();
            
            SerializationRepository.SerializedJsonCamelCasedIndented();
            */
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        //    .UseUrls("http://0.0.0.0:5000");
    }
}
