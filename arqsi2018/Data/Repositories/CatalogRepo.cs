using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Catalog;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Services;
using arqsi2018.Models.Catalog;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Product;
using Microsoft.EntityFrameworkCore;

namespace arqsi2018.Data.Repositories
{
    public class CatalogRepo : RepoBase<CatalogReadDto, CatalogWriteDto, Catalog>
    {
        private readonly ApiContext _context;
        private readonly CatalogAssembler _cAssembler;

        public CatalogRepo(ApiContext context)
        {
            _context = context;
            _cAssembler = new CatalogAssembler();
        }

        
        /// <summary>
        /// GET ALL
        /// </summary>
        /// <returns>IEnumerable CatalogReadDto</returns>
        public IEnumerable<CatalogReadDto> GetAll()
        {
            return _context.Catalogs.Include(x => x.ProductCatalogs).Select(x => _cAssembler.ToDto(x)).ToList();
        }

        /// <summary>
        /// POST
        /// </summary>
        /// <param name="writeDto"></param>
        /// <returns></returns>
        public Catalog Add(CatalogWriteDto writeDto)
        {
            var catalog = _cAssembler.ToDomainObject(writeDto);
            catalog.ProductCatalogs = new List<ProductCatalog>();
            var result = _context.Add(catalog).Entity;
            _context.SaveChanges();

            if (writeDto.Products == null) return result;
            
            result.ProductCatalogs = GetProductCatalogs(result, writeDto);
            _context.SaveChanges();

            return result;
        }

        /// <summary>
        /// GET BY ID
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>CatalogReadDto</returns>
        public CatalogReadDto GetById(int id)
        {
            var catalog = GetCatalogById(id);
            return catalog == null ? new CatalogReadDto() : _cAssembler.ToDto(catalog);
        }
        
        /// <summary>
        /// GET PRODUCTS BY CATALOG ID
        /// </summary>
        /// <param catalogId="id">int</param>
        /// <returns>ProductReadDto</returns>
        public IEnumerable<ProductReadDto> GetProductsByCatalogId(int id)
        {
            return new ProductRepo(_context).GetByCatalogId(id);
        }
        
        /// <summary>
        /// GET BY NAME
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>CatalogReadDto</returns>
        public CatalogReadDto GetByName(string Name)
        {
            var catalog = GetCatalogByName(Name);
            return catalog == null ? new CatalogReadDto() : _cAssembler.ToDto(catalog);
        }

        /// <summary>
        /// PUT
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="writeDto">CatalogWriteDto</param>
        /// <returns>CatalogReadDto</returns>
        public CatalogReadDto UpdateElement(int id, CatalogWriteDto writeDto)
        {
            var catalog = GetCatalogById(id);
            catalog.Name = writeDto.Name;
            catalog.Description = writeDto.Description;
            catalog.Date = DateTime.Parse(writeDto.Date);

            if (writeDto.Products == null)
            {
                return GetById(id); 
            }
            catalog.ProductCatalogs = GetProductCatalogs(catalog, writeDto);
 
            _context.SaveChanges();

            return GetById(id);
        }
        
        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>CatalogReadDto</returns>
        public CatalogReadDto DeleteElement(int id)
        {
            var toDelete = GetCatalogById(id);
            _context.Remove(toDelete);
            _context.SaveChanges();
            return _cAssembler.ToDto(toDelete);
        }
        
        /// <summary>
        /// GET CATALOG BY ID - private aux method
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>Catalog</returns>
        private Catalog GetCatalogById(int id)
        {
            return _context.Catalogs.Include(x => x.ProductCatalogs).ToList().FirstOrDefault(x => x.CatalogId == id);
        }
        
        
        private ICollection<ProductCatalog> GetProductCatalogs(Catalog catalog, CatalogWriteDto writeDto)
        {
            catalog.ProductCatalogs = new List<ProductCatalog>();
            _context.SaveChanges();
            
            var productCatalogs = new List<ProductCatalog>();

            foreach (var id in writeDto.Products)
            {
                var product = _context.Products.Find(id);
                var productCatalog = _context.ProductCatalogs.Add(
                    new ProductCatalog
                    {
                        Catalog = catalog,
                        Product = product,
                        CatalogId = catalog.CatalogId,
                        ProductId = product.ProductId
                    }
                ).Entity;
       
                _context.SaveChanges();
                    
                productCatalogs.Add(productCatalog);
            }

            return productCatalogs;
        }
        
        private Catalog GetCatalogByName(string Name)
        {
            return _context.Catalogs
                .Include(x => x.ProductCatalogs)
                .ToList().FirstOrDefault(x => x.Name == Name);
        }
    }
}