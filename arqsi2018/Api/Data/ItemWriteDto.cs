using System.Collections.Generic;

namespace arqsi2018.Controllers
{
    public class ItemWriteDto
    {
        public int ProductId { get; set; }
        public int MaterialId { get; set; }
        public int FinishId { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int ParentId { get; set; }
    }

    public class ItemsList
    {
        public List<ItemWriteDto> items { get; set; }
    }
}