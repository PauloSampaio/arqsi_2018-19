using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Models.Dimensions;

namespace arqsi2018.Data.Repositories
{
    public class DimensionsRepo : RepoBase<DimensionsReadDto,DimensionsWriteDto,Dimensions>
    {
        private readonly ApiContext _context;
        private readonly DimensionsAssembler _assembler;

        public DimensionsRepo(ApiContext context)
        {
            _context = context;
            _assembler = new DimensionsAssembler();
        }

        public IEnumerable<DimensionsReadDto> GetAll()
        {
            return _context.Dimensionses.ToList()
                .Select(x => _assembler.ToDto(x)).ToList();
        }

        public Dimensions Add(DimensionsWriteDto writeDto)
        {
            var dimensions = _assembler.ToDomainObject(writeDto);
            var result = _context.Add(dimensions).Entity;
            _context.SaveChanges();
            return result;
        }

        public DimensionsReadDto GetById(int id)
        {
            var dimensions = GetDimensionsById(id);
            //Todo: Better not found handling
            return dimensions == null ? new DimensionsReadDto() : _assembler.ToDto(dimensions);
        }

        public DimensionsReadDto UpdateElement(int id, DimensionsWriteDto writeDto)
        {
            var dimensions = GetDimensionsById(id);
            
            dimensions.LengthMin = writeDto.LengthMin;
            dimensions.LengthMax = writeDto.LengthMax;
            
            dimensions.WidthMin = writeDto.WidthMin;
            dimensions.WidthMax = writeDto.WidthMax;
            
            dimensions.HeightMin = writeDto.HeightMin;
            dimensions.HeightMax = writeDto.HeightMax;
            
            _context.SaveChanges();
            return GetById(id);
        }

        public DimensionsReadDto DeleteElement(int id)
        {
            var toDelete = GetDimensionsById(id);
            _context.Remove(toDelete);
            _context.SaveChanges();
            return _assembler.ToDto(toDelete);
        }
        
        private Dimensions GetDimensionsById(int id)
        {
            return _context.Dimensionses.Find(id);
        }
    }
}