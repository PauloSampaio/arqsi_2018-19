namespace arqsi2018.Models.Restriction
{
    public interface IRestrictionStrategy
    {
        bool ValidateRestrictions(Restriction restricton);
    }
}