using System.Collections.Generic;

namespace arqsi2018.Models.Material
{
    public class MaterialFactory
    {
        public static Material Create(string name, string description)
        {
            return new Material { Name = name, Description = description};
        }
    }
}