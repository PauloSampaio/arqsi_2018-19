using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using arqsi2018.Models;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Material;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace arqsi2018.Controllers
{
    [Route("api/material")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly MaterialService _service;

        public MaterialController(ApiContext context)
        {
            _service = new MaterialService(new MaterialRepo(context));
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<MaterialReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(MaterialReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }
        
        [HttpGet("name={name}")]
        [ProducesResponseType(200, Type = typeof(MaterialReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetByName(string name)
        {
            return Ok(_service.FindByName(name));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(MaterialReadDto))]
        public ActionResult Create([FromBody] MaterialWriteDto writeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            MaterialReadDto result = _service.AddElement(writeDto);

            return Created($"api/materials/{result.Id}", result);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id, [FromBody] MaterialWriteDto writeDto)
        {
            MaterialReadDto result = _service.UpdateMaterial(id, writeDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int id)
        {
            MaterialReadDto result = _service.DeleteMaterial(id);
            return Ok(result);
        }
    }
}