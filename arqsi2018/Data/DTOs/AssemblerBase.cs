namespace arqsi2018.Data.DTOs
{
    public interface AssemblerBase<TReadDto, TWriteDto, TEntityy>
    {
        TReadDto ToDto(TEntityy baseObject);
        TEntityy ToDomainObject(TWriteDto dto);
    }
}