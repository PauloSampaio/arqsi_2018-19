using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Product
{
    public class ProductParentsReadDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }

        public ICollection<int> ParentProducts { get; set; }
    }
}