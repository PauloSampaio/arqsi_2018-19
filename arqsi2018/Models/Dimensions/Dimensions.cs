using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using arqsi2018.Models.Joining;
using Microsoft.AspNetCore.DataProtection.KeyManagement.Internal;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace arqsi2018.Models.Dimensions
{
    /// <summary>
    /// Dimensions class. Assuming  base as centimeters.
    /// Discrete values can be handled just the same as a range.
    /// The only difference is that for discrete values the Minimum = Maximum.
    /// </summary>
    public class Dimensions
    {    
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DimensionsId { get; set; }

        //TODO: Consider making these fields required
        private double _lengthMin;
        private double _lengthMax;

        private double _widthMin;
        private double _widthMax;
        
        private double _heightMin;
        private double _heightMax;
  
        public double LengthMin
        {
            get => _lengthMin;
            set => _lengthMin = GetMin(value, LengthMax);
        }

        public double LengthMax
        {
            get => _lengthMax;
            set => _lengthMax = GetMax(LengthMin, value);
        }

        public double WidthMin
        {
            get => _widthMin;
            set => _widthMin = GetMin(value, WidthMax);
        }
        
        public double WidthMax
        {
            get => _widthMax;
            set => _widthMax = GetMax(WidthMin, value);
        }

        public double HeightMin
        {
            get => _heightMin;
            set => _heightMin = GetMin(value, HeightMax);
        }
        public double HeightMax
        {
            get => _heightMax;
            set => _heightMax = GetMax(HeightMin, value);
        }
        
        public virtual ICollection<ProductDimensions> ProductDimensions { get; set; }
        
        /// <summary>
        /// Dimensions constructor.
        /// </summary>
        /// <param name="lengthMin">double</param>
        /// <param name="lengthMax">double</param>
        /// <param name="widthMin">double</param>
        /// <param name="widthMax">double</param>
        /// <param name="heightMin">double</param>
        /// <param name="heightMax">double</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the values are either smaller than zero or any maximum is smaller than minimum.</exception>
        public Dimensions(double lengthMin, double lengthMax, double widthMin, double widthMax, double heightMin, double heightMax)
        {
            if ( CheckMinMax(lengthMin,lengthMax) && CheckMinMax(widthMin,widthMax) && CheckMinMax(heightMin,heightMax) )
            {
                _lengthMin = lengthMin;         
                _lengthMax = lengthMax;

                _widthMin = widthMin;
                _widthMax = widthMax;
            
                _heightMin = heightMin;
                _heightMax = heightMax;
            }
            else
                throw new ArgumentOutOfRangeException();         
        }

        /// <summary>
        /// Checks if the current dimensions fit in other dimensions that are passed as parameter.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        /// <returns>bool</returns>
        public bool CheckFitsIn(Dimensions dimensions)
        {
            return LengthMin < dimensions.LengthMin && LengthMax < dimensions.LengthMax &&
                   WidthMin < dimensions.WidthMin && WidthMax < dimensions.WidthMax &&
                   HeightMin < dimensions.HeightMin && HeightMax < dimensions.HeightMax;
        }

        /// <summary>
        /// Checks if the current dimensions fill at least the same or more as other dimensions passed as parameter.
        /// This is mostly for setting custom restrictions where furniture needs to fill at least certain dimensions within a parent.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        /// <returns>bool</returns>
        public bool CheckFillsAtLeast(Dimensions dimensions)
        {
            return LengthMin >= dimensions.LengthMin && LengthMax >= dimensions.LengthMax &&
                   WidthMin >= dimensions.WidthMin && WidthMax >= dimensions.WidthMax &&
                   HeightMin >= dimensions.HeightMin && HeightMax >= dimensions.HeightMax; 
        }

        /// <summary>
        /// Receives target Base Product dimensions and the minimum percentage that the current dimensions are expected to fill.
        /// calculates partdimensions / productdimensions which essentially returns a % of ocupation of the base product
        /// if the ocupation is under the target % the check returns false.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        /// <param name="minpct">int</param>
        /// <returns>bool</returns>
        public bool CheckFillsMinPct(Dimensions dimensions, int minpct)
        {
            if (minpct < 0 || minpct > 100)
                return false;

            double pct = minpct;
            
            pct = pct / 100; // ex 50% : 50/100 = 0.5
            
            return (LengthMin / dimensions.LengthMin) >= pct && (LengthMax / dimensions.LengthMin) >= pct &&
                   (WidthMin / dimensions.WidthMin) >= pct && (WidthMax / dimensions.LengthMin) >= pct &&
                   (HeightMin / dimensions.HeightMin) >= pct && (HeightMax / dimensions.LengthMin) >= pct; 
        }
        
        /// <summary>
        /// Receives target Base Product dimensions and the maximum percentage that the current dimensions are expected to fill.
        /// calculates partdimensions / productdimensions which essentially returns a % of ocupation of the base product
        /// if the ocupation is over the target % the check returns false.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        /// <param name="maxpct">int</param>
        /// <returns>bool</returns>
        public bool CheckFillsMaxPct(Dimensions dimensions, int maxpct)
        {
            if (maxpct < 0 || maxpct > 100)
                return false;
            
            double pct = maxpct;

            pct = pct / 100; // ex 50% : 50/100 = 0.5
            

            return (LengthMin / dimensions.LengthMax) <= pct && (LengthMax / dimensions.LengthMax) <= pct &&
                   (WidthMin / dimensions.WidthMax) <= pct && (WidthMax / dimensions.WidthMax) <= pct &&
                   (HeightMin / dimensions.HeightMax) <= pct && (HeightMax / dimensions.HeightMax) <= pct; 
        }

        /// <summary>
        /// Verifies that minimum is smaller or equal than the maximum
        /// and that both values are bigger than 0.
        /// </summary>
        /// <param name="min">double</param>
        /// <param name="max">double</param>
        /// <returns>bool</returns>
        private static bool CheckMinMax(double min, double max)
        {       
            return min > 0 && max > 0 && min <= max ;
        }
        
        /// <summary>
        /// Used for the purpose of setting min values. Just to shorten set code.
        /// </summary>
        /// <param name="min">double</param>
        /// <param name="max">double</param>
        /// <returns>double</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the values are either smaller than zero or any maximum is smaller than minimum.</exception>
        private double GetMin(double min, double max)
        {
            if (!CheckMinMax(min, max))
                throw new ArgumentOutOfRangeException(nameof(min));
            
            return min;          
        }
        
        /// <summary>
        /// Used for the purpose of setting max values. Just to shorten set code.
        /// </summary>
        /// <param name="min">double</param>
        /// <param name="max">double</param>
        /// <returns>double</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the values are either smaller than zero or any maximum is smaller than minimum.</exception>
        private double GetMax(double min, double max)
        {
            if (!CheckMinMax(min, max))
                throw new ArgumentOutOfRangeException(nameof(max));
            
            return max;                     
        }
        
        /// <summary>
        /// Checkes whether the dimensions passed as parameter are within the valid range of the current dimensions.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        /// <returns>bool</returns>
        public bool CheckValidRange(Dimensions dimensions)
        {
            return LengthMin <= dimensions.LengthMin && LengthMax >= dimensions.LengthMax &&
                   WidthMin <= dimensions.WidthMin && WidthMax >= dimensions.WidthMax &&
                   HeightMin <= dimensions.HeightMin && HeightMax >= dimensions.HeightMax; 
        }
    }
}