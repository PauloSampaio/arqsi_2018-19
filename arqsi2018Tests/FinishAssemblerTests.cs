using System;
using System.Collections.Generic;
using System.IO;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Material;
using Microsoft.IdentityModel.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace arqsi2018Tests
{
    [TestClass]
    public class FinishAssemblerTests
    {
        private Finish _finish1, _finish2, _finish3, _finish4, _finish5;
        private string _str1, _str2, _str3;
        private FinishReadDto _finishReadDto;
        private FinishWriteDto _finishWriteDto;
        private FinishAssembler _assembler;

        [TestCleanup]
        public void TestClean()
        {          
            _finish1 = null;
            _finish2 = null;
            _finish3 = null;
            _finish4 = null;
            _finish5 = null;

            _str1 = null;
            _str2 = null;
            _str3 = null;

            _finishReadDto = null;
            _finishWriteDto = null;

            _assembler = null;
        }

        [TestInitialize]
        public void TestInit()
        {
            //Create Finishes
            _finish1 = FinishFactory.Create("Finish1", "Desc1");
            _finish2 = FinishFactory.Create("Finish2", "Desc2");
            _finish3 = FinishFactory.Create("Finish3", "Desc3");
            _finish4 = FinishFactory.Create("Finish4", "Desc4");
            _finish5 = FinishFactory.Create("Finish5", "Desc5");
            
            //Create Strings
            _str1 = "Valid Name Test 12345";
            _str2 = "";
            _str3 = " Invalid Start";

            //Create finishWriteDTO
            _finishWriteDto = new FinishWriteDto
            {
                Name = "NameDto", Description = "Desc1",
            };

            //Create MaterialAssembler
            _assembler = new FinishAssembler();
        }

        [TestMethod]
        public void ToDto()
        {
            _finishReadDto = _assembler.ToDto(_finish1);
            Assert.AreEqual(_finishReadDto.Name, _finish1.Name);
            Assert.AreEqual(_finishReadDto.Description, _finish1.Description);
        }

        [TestMethod]
        public void ToDomainObject()
        {
            _finish2 = null;
            _finish2 = _assembler.ToDomainObject(_finishWriteDto);
            Assert.AreEqual(_finish2.Name, "NameDto");
            Assert.AreEqual(_finish2.Description, "Desc1");
        }

        [TestMethod]
        public void NameChecking()
        {
            _finish5.Name = _str1;
           
            Assert.ThrowsException<InvalidDataException>(
                () => _finish5.Name = _str2
            );
       
            Assert.ThrowsException<InvalidDataException>(
                () => _finish5.Name = _str3
            );
        }

    }
}