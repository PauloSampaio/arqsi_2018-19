using System;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.Repositories;

namespace arqsi2018.Data.Services
{
    public class MaterialService
    {
        private readonly MaterialRepo _repo;

        public MaterialService(MaterialRepo repo)
        {
            _repo = repo;
        }

        public IEnumerable<MaterialReadDto> FindAll()
        {
            return _repo.GetAll();
        }

        public MaterialReadDto AddElement(MaterialWriteDto writeDto)
        {
            var material = _repo.Add(writeDto);
            return _repo.GetById(material.MaterialId);
        }

        public MaterialReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }

        public MaterialReadDto UpdateMaterial(int id, MaterialWriteDto writeDto)
        {
            return _repo.UpdateElement(id, writeDto);
        }

        public MaterialReadDto DeleteMaterial(int id)
        {
            return _repo.DeleteElement(id);
        }

        public MaterialReadDto FindByName(string Name)
        {
            return _repo.GetByName(Name);
        }
    }
}