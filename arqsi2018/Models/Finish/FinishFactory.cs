namespace arqsi2018.Models.Finish
{
    public class FinishFactory
    {
        public static Finish Create(string name, string description)
        {
            return new Finish {Name = name, Description = description};
        }
    }
}