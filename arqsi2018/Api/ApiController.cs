using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Api.Data;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace arqsi2018.Controllers
{
    [Route("api/validate")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly ApiService _service;
        
        public ApiController(ApiContext context)
        {
           _service = new ApiService(new ApiRepo(context));
        }
        
        [HttpPost]
        [ProducesResponseType(200)]
        public ActionResult ValidateItem(ItemsList itemsList)
        {
            var validationresult = _service.ValidateItems(itemsList);
            
            if(validationresult == -1)
            return Ok("Items successfully validated.");
            
            return BadRequest("There was an error on item: "+validationresult);
        }
    }
}