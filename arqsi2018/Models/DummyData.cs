using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Controllers;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Catalog;
using arqsi2018.Data.DTOs.Category;
using arqsi2018.Data.DTOs.Dimensions;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;
using arqsi2018.Models.Category;
using arqsi2018.Models.Dimensions;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Material;
using Microsoft.EntityFrameworkCore.Internal;
using static arqsi2018.Models.Finish.Finish;

namespace arqsi2018.Models
{
    public class DummyData
    {
        
        
        public static void Initialize(ApiContext context)
        {
            
            if (!context.Finishes.Any())
            {BuildFinishes(context);}
            
            if (!context.Materials.Any())
            {BuildMaterials(context);}
            
            if (!context.Categories.Any())
            {BuildCategories(context);}
            
            if (!context.Catalogs.Any())
            {BuildCatalogs(context);}
            
            if (!context.Products.Any())
            {BuildProducts(context);}
          
        }

        private static void BuildProducts(ApiContext context)
        {
            var ctr = new ProductController(context);

            ctr.Create(new ProductWriteDto
            {
                Catalogs = new List<int>{1},
                CategoryId = 1,
                Description = "desc",
             /*   DimensionsWriteDto = new DimensionsWriteDto
                {
                    HeightMax = 100,
                    HeightMin = 100,
                    LengthMax = 100,
                    LengthMin = 100,
                    WidthMax = 100,
                    WidthMin = 100
                },*/
                Materials = new List<int>{1,2},
                Name = "prod1",
                PartsIds = new List<int>(),
                Reference = "ref"
            });
            
            ctr.Create(new ProductWriteDto
            {
                Catalogs = new List<int>{1},
                CategoryId = 2,
                Description = "desc",
            /*    DimensionsWriteDto = new DimensionsWriteDto
                {
                    HeightMax = 100,
                    HeightMin = 100,
                    LengthMax = 100,
                    LengthMin = 100,
                    WidthMax = 100,
                    WidthMin = 100
                },*/
                Materials = new List<int>{1,2},
                Name = "prod2",
                PartsIds = new List<int>(),
                Reference = "ref"
            });
            
            ctr.Create(new ProductWriteDto
            {
                Catalogs = new List<int>{2},
                CategoryId = 3,
                Description = "desc",
            /*    DimensionsWriteDto = new DimensionsWriteDto
                {
                    HeightMax = 100,
                    HeightMin = 100,
                    LengthMax = 100,
                    LengthMin = 100,
                    WidthMax = 100,
                    WidthMin = 100
                },*/
                Materials = new List<int>{3},
                Name = "prod3",
                PartsIds = new List<int>(),
                Reference = "ref"
            });
        }
        
        private static void BuildCatalogs(ApiContext context)
        {
            var ctr = new CatalogController(context);

            ctr.Create(new CatalogWriteDto
            {
                Date = "01-01-2018",
                Description = "desc",
                Name = "catalog1",
                Products = new List<int>()
            });
            
            ctr.Create(new CatalogWriteDto
            {
                Date = "01-01-2018",
                Description = "desc",
                Name = "catalog2",
                Products = new List<int>()
            });
            
            ctr.Create(new CatalogWriteDto
            {
                Date = "01-01-2018",
                Description = "desc",
                Name = "catalog3",
                Products = new List<int>()
            });
        }
        
        private static void BuildFinishes(ApiContext context)
        {
            var ctr = new FinishController(context);
            
            ctr.Create(new FinishWriteDto
            {
                Name = "finish1", Description = "desc"
            });
            
            ctr.Create(new FinishWriteDto
            {
                Name = "finish2", Description = "desc"
            });
            
            ctr.Create(new FinishWriteDto
            {
                Name = "finish3", Description = "desc"
            });
            
            ctr.Create(new FinishWriteDto
            {
                Name = "finish4", Description = "desc"
            });
        }

        private static void BuildMaterials(ApiContext context)
        {
            var ctr = new MaterialController(context);
            
            ctr.Create(new MaterialWriteDto
            {
                Name = "mat2", Description = "desc"
            });
            
            ctr.Create(new MaterialWriteDto
            {
                Name = "mat3", Description = "desc"
            });

            var list = new List<int> {1, 3};
            ctr.Create(new MaterialWriteDto
            {
                Name = "mat4", Description = "desc", Finishes = list
            });
        }
        
        private static void BuildCategories(ApiContext context)
        {
            
            var ctr3 = new CategoryController(context);

            ctr3.Create(new CategoryWriteDto
            {
                Name = "cat1", Description = "desc", ParentId = 33
            });
            
            ctr3.Create(new CategoryWriteDto
            {
                Name = "cat2", Description = "desc", ParentId = 33
            });
            
            ctr3.Create(new CategoryWriteDto
            {
                Name = "cat3", Description = "desc", ParentId = 1
            });
            
            ctr3.Create(new CategoryWriteDto
            {
                Name = "cat4", Description = "desc", ParentId = 3
            });
            
            ctr3.Create(new CategoryWriteDto
            {
                Name = "cat5", Description = "desc", ParentId = 2
            });
        }
    }
}