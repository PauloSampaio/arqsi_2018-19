using System;

namespace arqsi2018.Models.Dimensions
{
    public class DimensionsFactory
    {
        public static Dimensions Create(double lengthMin, double lengthMax,
            double widthMin, double widthMax, double heightMin, double heightMax)
        {           
            return new Dimensions (lengthMin, lengthMax, widthMin, widthMax, heightMin, heightMax);
        }
    }
}