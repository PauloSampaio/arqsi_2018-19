using System;
using arqsi2018.Models;
using arqsi2018.Models.Dimensions;

namespace arqsi2018.Data.DTOs.Dimensions
{
    public class DimensionsAssembler : AssemblerBase<DimensionsReadDto, DimensionsWriteDto, Models.Dimensions.Dimensions>
    {
        public DimensionsReadDto ToDto(Models.Dimensions.Dimensions dimensions)
        {
            if(dimensions == null)
                return new DimensionsReadDto();
                    
            var dimensionsReadDto = new DimensionsReadDto
            {
                LengthMin = dimensions.LengthMin,
                LengthMax = dimensions.LengthMax,

                WidthMin = dimensions.WidthMin,
                WidthMax = dimensions.WidthMax,

                HeightMin = dimensions.HeightMin,
                HeightMax = dimensions.HeightMax,
                
                Id = dimensions.DimensionsId
            };

            return dimensionsReadDto;
        }

        public Models.Dimensions.Dimensions ToDomainObject(DimensionsWriteDto dto)
        {
            return DimensionsFactory.Create(dto.LengthMin, dto.LengthMax,
                dto.WidthMin, dto.WidthMax, dto.HeightMin, dto.HeightMax);
        }
    }
}