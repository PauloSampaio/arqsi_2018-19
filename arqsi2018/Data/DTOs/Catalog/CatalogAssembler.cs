using System;
using System.Collections.Generic;
using System.Globalization;
using arqsi2018.Models.Catalog;

namespace arqsi2018.Data.DTOs.Catalog
{
    public class CatalogAssembler : AssemblerBase<CatalogReadDto, CatalogWriteDto, Models.Catalog.Catalog>
    {
        public CatalogReadDto ToDto(Models.Catalog.Catalog catalog)
        {
            if(catalog == null)
                return new CatalogReadDto();
            
            var readDto = new CatalogReadDto();

            readDto.Id = catalog.CatalogId;
            readDto.Name = catalog.Name;
            readDto.Description = catalog.Description;
            readDto.Date = catalog.Date.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
            var productIds = new List<int>();


            // fills productIds 
            if (catalog.ProductCatalogs == null)
            {
                readDto.Products = productIds;
            }
            else
            {
                foreach (var x in catalog.ProductCatalogs)
                {
                    productIds.Add(x.ProductId);
                }
                readDto.Products = productIds;
            }
            
            return readDto;
        }

        public Models.Catalog.Catalog ToDomainObject(CatalogWriteDto dto)
        {
            var dt = DateTime.Parse(dto.Date);
            return CatalogFactory.Create(dto.Name,dto.Description,dt);
        }
    }
}