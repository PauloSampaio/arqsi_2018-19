using System;
using System.Collections.Generic;
using System.IO;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Material;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace arqsi2018Tests
{
    [TestClass]
    public class MaterialAssemblerTests
    {
        private Material _material1, _material2;
        private string _str1, _str2, _str3;
        private MaterialReadDto _materialReadDto;
        private MaterialWriteDto _materialWriteDto;
        private MaterialAssembler _assembler;
        
        [TestCleanup]  
        public void TestClean()
        {
            _str1 = null;
            _str2 = null;
            _str3 = null; 
            
            _material1 = null;
            _material2 = null;
            
            _materialReadDto = null;
            _materialWriteDto = null;
            
            _assembler = null;
        }  

        [TestInitialize]  
        public void TestInit()
        {
            //Create materials
            _material1 = MaterialFactory.Create("Material_1", "Desc1");
            _material2 = MaterialFactory.Create("Material_2", "Desc2");
            
            //Create Strings
            _str1 = "Valid Name Test 12345";
            _str2 = "";
            _str3 = " Invalid Start";
            
            //Create materialWriteDTO
            _materialWriteDto = new MaterialWriteDto
            {
                Name = "NameDto", Description = "Desc1", Finishes = new List<int>()
            };
            
            //Create MaterialAssembler
            _assembler = new MaterialAssembler();
        }  

        [TestMethod]
        public void ToDto()
        {
            _materialReadDto = _assembler.ToDto(_material1);
            Assert.AreEqual(_materialReadDto.Name, _material1.Name);
            Assert.AreEqual(_materialReadDto.Description, _material1.Description);
        }

        [TestMethod]
        public void ToDomainObject()
        {
            _material2 = null;
            _material2 = _assembler.ToDomainObject(_materialWriteDto);
            Assert.AreEqual(_material2.Name, "NameDto");
            Assert.AreEqual(_material2.Description, "Desc1");
        }
        
        [TestMethod]
        public void NameChecking()
        {
            _material1.Name = _str1;
           
            Assert.ThrowsException<InvalidDataException>(
                () => _material1.Name = _str2
            );
       
            Assert.ThrowsException<InvalidDataException>(
                () => _material1.Name = _str3
            );
        }
    }
}