namespace arqsi2018.Models.Joining
{
    public class ProductCatalog
    {
        public int ProductId { get; set; }
        public Product.Product Product { get; set; }
        public int CatalogId { get; set; }
        public Catalog.Catalog Catalog { get; set; }
    }
}