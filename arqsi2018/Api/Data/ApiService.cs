using System;
using arqsi2018.Api.Data;
using arqsi2018.Controllers;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;

namespace arqsi2018.Data.Services
{
    public class ApiService
    {
        private readonly ApiRepo _repo;

        public ApiService(ApiRepo repo)
        {
            _repo = repo;
        }

        public int ValidateItems(ItemsList itemsList)
        {
            return _repo.ValidateItems(itemsList);
        }
    }  
}