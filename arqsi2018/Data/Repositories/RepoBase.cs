using System;
using System.Collections;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Finish;

namespace arqsi2018.Data.Repositories
{
    public interface RepoBase<TReadDto, TWriteDto, TEntityy>
    {
        IEnumerable<TReadDto> GetAll();
        TEntityy Add(TWriteDto writeDto);
        TReadDto GetById(int id);
        TReadDto UpdateElement(int id, TWriteDto writeDto);
        TReadDto DeleteElement(int id);
    }
}