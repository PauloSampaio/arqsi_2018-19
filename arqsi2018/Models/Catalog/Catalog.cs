using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Text.RegularExpressions;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Product;

namespace arqsi2018.Models.Catalog
{
    public class Catalog
    {     
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CatalogId { get; set; }

        private string _name;
        [Required]
        public string Name {get => _name;set => _name = CheckName(value);}   
        public string Description { get; set; }
        [Required]
        [Column(TypeName = "DateTime")]
        public DateTime Date  { get; set; } 
        
        public virtual ICollection<ProductCatalog> ProductCatalogs { get; set; }

        /// <summary>
        /// Checks if the name input is valid.
        /// Input must start with at least a character or number.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>string</returns>
        /// <exception cref="InvalidDataException"></exception>
        private string CheckName(string name)
        {
            var pattern = new Regex("^[0-9A-Za-z][0-9A-Za-z ]*");
            
            if (!pattern.IsMatch(name))
                throw new InvalidDataException(nameof(name));
            
            return name;          
        }
    }
}