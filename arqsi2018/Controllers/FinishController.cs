using System;
using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace arqsi2018.Controllers
{
    [Route("api/finish")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class FinishController : ControllerBase
    {
        private readonly FinishService _service;

        public FinishController(ApiContext context)
        {
            _service = new FinishService(new FinishRepo(context));
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<FinishReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(FinishReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }
        
        [HttpGet("name={name}")]
        [ProducesResponseType(200, Type = typeof(FinishReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetByName(string name)
        {
            return Ok(_service.FindByName(name));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(FinishReadDto))]
        public ActionResult Create([FromBody] FinishWriteDto finishWriteDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FinishReadDto result = _service.AddElement(finishWriteDto);

            return Created($"api/finishes/{result.Id}", result);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id,[FromBody] FinishWriteDto finishWriteDto)
        {
            FinishReadDto result = _service.UpdateFinish(id, finishWriteDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int id)
        {
            FinishReadDto result = _service.DeleteFinish(id);
            return Ok(result);
        }
    }
}