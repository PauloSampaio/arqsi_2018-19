namespace arqsi2018.Models.Product
{
    public class ProductFactory
    {
        public static Product Create(string Name, string Description, string Reference)
        {
            return new Product
            {
                Name = Name,
                Description = Description,
                Reference = Reference
            };
        }
    }
}