namespace arqsi2018.Models.Joining
{
    public class MaterialFinish
    {
        public int MaterialId { get; set; }
        public Material.Material Material { get; set; }
        public int FinishId { get; set; }
        public Finish.Finish Finish { get; set; }
    }
}