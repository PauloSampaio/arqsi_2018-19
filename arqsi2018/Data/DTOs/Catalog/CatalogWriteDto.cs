using System;
using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Catalog
{
    public class CatalogWriteDto
    {   
        public string Name { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        
        public List<int> Products { get; set; }
    }
}