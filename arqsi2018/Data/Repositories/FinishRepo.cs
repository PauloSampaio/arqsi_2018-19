using System;
using System.Collections.Generic;
using System.Linq;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Models.Finish;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Data.Repositories
{
    public class FinishRepo : RepoBase<FinishReadDto,FinishWriteDto,Finish>
    {
        private readonly ApiContext _context;
        private readonly FinishAssembler _assembler;
        
        public FinishRepo(ApiContext context)
        {
            _context = context;
            _assembler = new FinishAssembler();
        }
        
        /**
         *
         ***************************************
         *            GET_ALL                  *
         ***************************************
         * 
         */
        public IEnumerable<FinishReadDto> GetAll()
        {
            return _context.Finishes.ToList()
                .Select(x => _assembler.ToDto(x)).ToList();
        }

        /**
         *
         ***************************************
         *            POST                     *
         ***************************************
         * 
         */
        public Finish Add(FinishWriteDto finishWriteDto)
        {
            var finish = _assembler.ToDomainObject(finishWriteDto);
            var result = _context.Finishes.Add(finish).Entity;
            _context.SaveChanges();
            return result;
        }

        /**
         *
         ***************************************
         *            GET_BY_ID                *
         ***************************************
         * 
         */
        public FinishReadDto GetById(int id)
        {
            var finish = GetFinishById(id);
            //Todo: Better not found handling
            return finish == null ? new FinishReadDto() : _assembler.ToDto(finish);
        }
        
        /**
         *
         ***************************************
         *            GET_BY_NAME               *
         ***************************************
         * 
         */
        public FinishReadDto GetByName(string Name)
        {
            return _assembler.ToDto(GetFinishByName(Name));
        }
        
        /**
         *
         ***************************************
         *                PUT                  *
         ***************************************
         * 
         */
        public FinishReadDto UpdateElement(int id, FinishWriteDto finishWriteDto)
        {
            var finish = GetFinishById(id);

            if (finishWriteDto.Name != null)
            {
                finish.Name = finishWriteDto.Name;
            }

            if (finishWriteDto.Description != null)
            {
                finish.Description = finishWriteDto.Description;
            }
            
            _context.SaveChanges();
            return GetById(id);
        }

        /**
         *
         ***************************************
         *                DEL                  *
         ***************************************
         * 
         */
        public FinishReadDto DeleteElement(int id)
        {
            var toDelete = GetFinishById(id);
            _context.Remove(toDelete);
            _context.SaveChanges();
            return _assembler.ToDto(toDelete);
        }
        
        
        private Finish GetFinishById(int finishId)
        {
            return _context.Finishes.Find(finishId);
        }
        
        private Finish GetFinishByName(string Name)
        {
            return _context.Finishes
                .FirstOrDefault(x => x.Name == Name);
        }
    }
}