using System.Collections.Generic;

namespace arqsi2018.Data.DTOs.Category
{
    public class CategoryReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        
        public List<int> Children { get; set; }
    }
}