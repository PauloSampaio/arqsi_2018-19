using System.Collections.Generic;
using arqsi2018.Data.DTOs.Dimensions;

namespace arqsi2018.Data.DTOs.Product
{
    public class ProductWriteDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }    
        public int? CategoryId { get; set; }
        
        public ICollection<int> PartsIds { get; set; }
        public ICollection<int> Catalogs { get; set; }
        public ICollection<int> Materials { get; set; }
        public ICollection<int> Dimensionses { get; set; }
        
        public ICollection<NestedRestrictions> NestedRestrictions { get; set; }
    }

    public class NestedRestrictions
    {
        public int IsRequired  { get; set; }
        public int UseBaseProductMaterial { get; set; }
        public int MinDimensions { get; set; }
        public int MaxDimensions { get; set; }    
    }
}