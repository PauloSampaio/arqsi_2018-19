using System;
using System.Collections;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Catalog;
using arqsi2018.Data.DTOs.Product;
using arqsi2018.Data.Repositories;

namespace arqsi2018.Data.Services
{
    public class CatalogService
    {
        private readonly CatalogRepo _repo;
        
        public CatalogService(CatalogRepo repo)
        {
            _repo = repo;
        }

        public IEnumerable<CatalogReadDto> FindAll()
        {
            return _repo.GetAll();
        }

        public CatalogReadDto AddElement(CatalogWriteDto writeDto)
        {
            var catalog = _repo.Add(writeDto);
            return _repo.GetById(catalog.CatalogId);
        }
        
        public CatalogReadDto FindById(int id)
        {
            return _repo.GetById(id);
        }

        public IEnumerable<ProductReadDto> FindProducts(int id)
        {
            return _repo.GetProductsByCatalogId(id);
            throw new Exception("Not implemented yet");
        }

        public CatalogReadDto UpdateCatalog(int id, CatalogWriteDto writeDto)
        {
            return _repo.UpdateElement(id, writeDto);
        }

        public CatalogReadDto DeleteCatalog(int id)
        {
            return _repo.DeleteElement(id);
        }

        public CatalogReadDto FindByName(string Name)
        {
            return _repo.GetByName(Name);
        }
    }
}