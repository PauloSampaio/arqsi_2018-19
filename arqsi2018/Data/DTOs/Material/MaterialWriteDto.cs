using System.Collections.Generic;
using arqsi2018.Data.DTOs.Finish;

namespace arqsi2018.Data.DTOs.Material
{
    public class MaterialWriteDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public List<int> Finishes { get; set; }
    }
}