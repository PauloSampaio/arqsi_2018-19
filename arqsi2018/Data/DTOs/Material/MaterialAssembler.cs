using System;
using System.Collections.Generic;
using arqsi2018.Data.DTOs.Finish;
using arqsi2018.Models.Material;

namespace arqsi2018.Data.DTOs.Material
{
    public class MaterialAssembler : AssemblerBase<MaterialReadDto, MaterialWriteDto, Models.Material.Material>
    {
        public MaterialReadDto ToDto(Models.Material.Material material)
        {
            if(material == null)
                return new MaterialReadDto();
            
            var readDto = new MaterialReadDto();
            
            readDto.Id = material.MaterialId;
            readDto.Name = material.Name;
            readDto.Description = material.Description;
            var finishesIds = new List<int>();


            if (material.MaterialFinishes == null)
            {
                readDto.Finishes = finishesIds;
            }
            else
            {
                foreach (var x in material.MaterialFinishes)
                {
                    finishesIds.Add(x.FinishId);
                }

                readDto.Finishes = finishesIds;
            }
            
            
            return readDto;

        }

        public Models.Material.Material ToDomainObject(MaterialWriteDto dto)
        {
            return new Models.Material.Material{Name = dto.Name, Description = dto.Description}; 
        }
    }
}