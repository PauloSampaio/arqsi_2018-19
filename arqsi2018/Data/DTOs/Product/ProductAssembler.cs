using System;
using System.Collections.Generic;
using arqsi2018.Models.Product;

namespace arqsi2018.Data.DTOs.Product
{
    public class ProductAssembler : AssemblerBase<ProductReadDto, ProductWriteDto, Models.Product.Product>
    {
        public ProductReadDto ToDto(Models.Product.Product product)
        {
            if(product == null)
                return new ProductReadDto();
            
            var readDto = new ProductReadDto();

            readDto.ProductId = product.ProductId;
            readDto.Name = product.Name;
            readDto.Description = product.Description;
            readDto.Reference = product.Reference;
            readDto.CategoryId = product.Category.CategoryId;

            var partIds = new List<int>();
            var catalogIds = new List<int>();
            var materialIds = new List<int>();
            var dimensionsIds = new List<int>();
            
            // fills Part ids
            if (product.OwnedParts == null)
            {
                readDto.PartsIds = partIds;
            }
            else
            {
                foreach (var x in product.OwnedParts)
                {
                    partIds.Add(x.PartId);
                }
                readDto.PartsIds = partIds;
            }
            
            // fills Catalog ids
            if (product.ProductCatalogs == null)
            {
                readDto.Catalogs = catalogIds;
            }
            else
            {
                foreach (var x in product.ProductCatalogs)
                {
                    catalogIds.Add(x.CatalogId);
                }
                readDto.Catalogs = catalogIds;
            }
            
            // fills Material ids
            if (product.ProductMaterials == null)
            {
                readDto.Materials = materialIds;
            }
            else
            {
                foreach (var x in product.ProductMaterials)
                {
                    materialIds.Add(x.MaterialId);
                }
                readDto.Materials = materialIds;
            }
            
            // fills Dimensions ids
            if (product.ProductDimensions == null)
            {
                readDto.Dimensionses = dimensionsIds;
            }
            else
            {
                foreach (var x in product.ProductDimensions)
                {
                    dimensionsIds.Add(x.DimensionsId);
                }
                readDto.Dimensionses = dimensionsIds;
            }

            return readDto;
        }

        public Models.Product.Product ToDomainObject(ProductWriteDto dto)
        {
            return ProductFactory.Create(dto.Name, dto.Description, dto.Reference);
        }
        
        /// <summary>
        /// To ProductPartsReadDto
        /// </summary>
        /// <param name="product">Product</param>
        /// <returns>ProductPartsReadDto</returns>
        public ProductPartsReadDto ToPartsDto(Models.Product.Product product)
        {
            var readDto = new ProductPartsReadDto();

            readDto.ProductId = product.ProductId;
            readDto.Name = product.Name;

            var partIds = new List<int>();
            
            // fills Part ids
            if (product.OwnedParts == null)
            {
                readDto.PartsIds = partIds;
            }
            else
            {
                foreach (var x in product.OwnedParts)
                {
                    partIds.Add(x.PartId);
                }
                readDto.PartsIds = partIds;
            }

            return readDto;
        }
        
        /// <summary>
        /// To ProductParentsReadDto
        /// </summary>
        /// <param name="product">Product</param>
        /// <returns>ProductPartsReadDto</returns>
        public ProductParentsReadDto ToParentsReadDto(Models.Product.Product product)
        {
            var readDto = new ProductParentsReadDto();

            readDto.ProductId = product.ProductId;
            readDto.Name = product.Name;

            var parentIds = new List<int>();
            
            // fills Part ids
            if (product.ParentProducts == null)
            {
                readDto.ParentProducts = parentIds;
            }
            else
            {
                foreach (var x in product.ParentProducts)
                {
                    parentIds.Add(x.ProductId);
                }
                readDto.ParentProducts = parentIds;
            }

            return readDto;
        }
        
        /// <summary>
        /// To RestrictionReadDto
        /// </summary>
        /// <param name="product">Restriction</param>
        /// <returns>RestrictionReadDto</returns>
        public RestrictionReadDto ToRestrictionDto(Models.Restriction.Restriction restriction)
        {
            var readDto = new RestrictionReadDto();
            
            readDto.ProductId = restriction.ProductId;
            readDto.Name = restriction.Product.Name;
            readDto.PartId = restriction.PartId;
            readDto.PartName = restriction.Part.Name;
            readDto.IsRequired = restriction.IsRequired;
            readDto.UseBaseProductMaterial = restriction.UseBaseProductMaterial;
            readDto.MinDimensions = restriction.MinDimensions;
            readDto.MaxDimensions = restriction.MaxDimensions;
            
            return readDto;
        }

        /// <summary>
        /// Validates whether the nested restrictions part of the dto is existent or properly formed, otherwise they are ignored
        /// </summary>
        /// <param name="dto">ProductWriteDto</param>
        /// <returns>bool</returns>
        private bool ValidateRestrictionData(ProductWriteDto dto)
        {
            if (dto.NestedRestrictions == null)
                return false;
            
            if(dto.PartsIds.Count != dto.NestedRestrictions.Count)
                return false;
            
            foreach (var restrictiondata in dto.NestedRestrictions)
            {
                if (!(restrictiondata.IsRequired == 0 || restrictiondata.IsRequired == 1))
                    return false;
                if (!(restrictiondata.UseBaseProductMaterial == 0 || restrictiondata.UseBaseProductMaterial == 1))
                    return false;
                if (!(restrictiondata.MinDimensions >= 0 || restrictiondata.MinDimensions < 100))
                    return false;
                if (!(restrictiondata.MaxDimensions >= 0 || restrictiondata.MaxDimensions < 100))
                    return false;
            }
            return true;
        }

        public ICollection<RestrictionReadDto> ToRestrictionsPartialDto(ProductWriteDto dto)
        {   
            var dtoList = new List<RestrictionReadDto>();
            
            // will load default values
            if (!ValidateRestrictionData(dto)) 
            {
                foreach (var var in dto.PartsIds)
                {
                    var restDto = new RestrictionReadDto();

                    restDto.IsRequired = false;
                    restDto.UseBaseProductMaterial = false;
                    restDto.MinDimensions = 0;
                    restDto.MaxDimensions = 0;

                    dtoList.Add(restDto);
                }
                return dtoList;
            }

            // loads values from nestedrestrictions
            foreach (var restrictiondata in dto.NestedRestrictions)
            {
                var restDto = new RestrictionReadDto();

                restDto.IsRequired = restrictiondata.IsRequired == 1;
                restDto.UseBaseProductMaterial = restrictiondata.UseBaseProductMaterial == 1;
                restDto.MinDimensions = restrictiondata.MinDimensions;
                restDto.MaxDimensions = restrictiondata.MaxDimensions;

                dtoList.Add(restDto);
            }
            return dtoList;
        } 
    }
}