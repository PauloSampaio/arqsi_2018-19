using System;

namespace arqsi2018.Data.DTOs.Finish
{
    public class FinishReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}