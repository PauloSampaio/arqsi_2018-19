using System;
using System.Data.SqlClient;
using arqsi2018.Models;
using arqsi2018.Models.Catalog;
using arqsi2018.Models.Category;
using arqsi2018.Models.Finish;
using arqsi2018.Models.Material;
using arqsi2018.Models.Dimensions;
using arqsi2018.Models.Joining;
using arqsi2018.Models.Product;
using arqsi2018.Models.Restriction;
using Microsoft.EntityFrameworkCore;

namespace arqsi2018.Data.Context
{
    public class ApiContext : DbContext
    {
        private static string _connection = "Server=tcp:sicbackoffice.database.windows.net,1433;" +
                                    "Database=sic_backoffice;" +
                                    "Uid=admin1@sicbackoffice;" +
                                    "Pwd=Pa22word";
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        { }
        
        public DbSet<Finish> Finishes { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Dimensions> Dimensionses { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<Restriction> Restrictions { get; set; }
        public DbSet<ProductCatalog> ProductCatalogs { get; set; }
        public DbSet<ProductMaterial> ProductMaterials { get; set; }
        public DbSet<MaterialFinish> MaterialFinishes { get; set; }
        public DbSet<ProductDimensions> ProductDimensions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connection);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Restriction>().HasKey(pp => new { pp.ProductId, pp.PartId });
            modelBuilder.Entity<ProductCatalog>().HasKey(pc => new { pc.ProductId, pc.CatalogId });
            modelBuilder.Entity<ProductMaterial>().HasKey(pm => new { pm.ProductId, pm.MaterialId});
            modelBuilder.Entity<MaterialFinish>().HasKey(mf => new { mf.MaterialId, mf.FinishId});
            modelBuilder.Entity<ProductDimensions>().HasKey(pd => new {pd.ProductId, pd.DimensionsId});
            modelBuilder.Entity<Category>().HasKey(c => c.CategoryId);
                
            modelBuilder.Entity<Restriction>()
                .HasOne(pp => pp.Product)
                .WithMany(p => p.OwnedParts)
                .HasForeignKey(pp => pp.ProductId).OnDelete(DeleteBehavior.Restrict); ;         
            modelBuilder.Entity<Restriction>()
                .HasOne(pp => pp.Part)
                .WithMany(p => p.ParentProducts)
                .HasForeignKey(pp => pp.PartId).OnDelete(DeleteBehavior.Restrict); ;
         
            modelBuilder.Entity<ProductCatalog>()
                .HasOne(pc => pc.Product)
                .WithMany(p => p.ProductCatalogs)
                .HasForeignKey(pc => pc.ProductId); 
            modelBuilder.Entity<ProductCatalog>()
                .HasOne(pc => pc.Catalog)
                .WithMany(c => c.ProductCatalogs)
                .HasForeignKey(pc => pc.CatalogId);
            
            modelBuilder.Entity<ProductMaterial>()
                .HasOne(pm => pm.Product)
                .WithMany(p => p.ProductMaterials)
                .HasForeignKey(pm => pm.ProductId); 
            modelBuilder.Entity<ProductMaterial>()
                .HasOne(pm => pm.Material)
                .WithMany(m => m.ProductMaterials)
                .HasForeignKey(pm => pm.MaterialId);
            
            modelBuilder.Entity<MaterialFinish>()
                .HasOne(mf => mf.Material)
                .WithMany(m => m.MaterialFinishes)
                .HasForeignKey(mf => mf.MaterialId);
            modelBuilder.Entity<MaterialFinish>()
                .HasOne(mf => mf.Finish)
                .WithMany(f => f.MaterialFinishes)
                .HasForeignKey(mf => mf.FinishId); 
            
            modelBuilder.Entity<ProductDimensions>()
                .HasOne(pd => pd.Product)
                .WithMany(p => p.ProductDimensions)
                .HasForeignKey(pd => pd.ProductId);
            modelBuilder.Entity<ProductDimensions>()
                .HasOne(pd => pd.Dimensions)
                .WithMany(d => d.ProductDimensions)
                .HasForeignKey(pd => pd.DimensionsId);
            
            modelBuilder.Entity<Category>().HasOne(p => p.Parent)
                .WithMany().HasForeignKey(c => c.ParentId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}