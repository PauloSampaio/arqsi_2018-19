using System.Collections.Generic;
using arqsi2018.Data.Context;
using arqsi2018.Data.DTOs.Catalog;
using arqsi2018.Data.DTOs.Material;
using arqsi2018.Data.Repositories;
using arqsi2018.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace arqsi2018.Controllers
{
    [Route("api/catalog")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly CatalogService _service;

        public CatalogController(ApiContext context)
        {
            _service = new CatalogService(new CatalogRepo(context));
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CatalogReadDto>))]
        public ActionResult GetAll()
        {
            return Ok(_service.FindAll());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(CatalogReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetById(int id)
        {
            return Ok(_service.FindById(id));
        }

        // EndPoint para NodeJs >> Interface
        [HttpGet("{id}/products")]
        [ProducesResponseType(200)]
        public ActionResult GetProductsById(int id)
        {
            return Ok(_service.FindProducts(id));
        }
        
        [HttpGet("name={name}")]
        [ProducesResponseType(200, Type = typeof(CatalogReadDto))]
        [ProducesResponseType(404)]
        public ActionResult GetByName(string name)
        {
            return Ok(_service.FindByName(name));
        }
        
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(CatalogReadDto))]
        public ActionResult Create([FromBody] CatalogWriteDto writeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            CatalogReadDto result = _service.AddElement(writeDto);

            return Created($"api/catalog/{result.Id}", result);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        public ActionResult Update(int id, [FromBody] CatalogWriteDto writeDto)
        {
            CatalogReadDto result = _service.UpdateCatalog(id, writeDto);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int id)
        {
            CatalogReadDto result = _service.DeleteCatalog(id);
            return Ok(result);
        }
    }
}